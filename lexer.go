// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "fmt"
import "bufio"
import "strings"
import "unicode"
import "unicode/utf8"

type TokenKind int

const (
	TOK_EOF TokenKind = iota
	TOK_ERROR

	// simple tokens
	TOK_Int
	TOK_Float
	TOK_Char
	TOK_String

	TOK_Name
	TOK_Module

	// compound tokens
	TOK_Expr
	TOK_Access
	TOK_Data
)

type Token struct {
	Kind     TokenKind
	Children []*Token
	Str      string
	Module   string // for TOK_Name: preceding module name (if any)
	LineNum  int
	Quoted   bool
	Info     *NodeInfo
}

const ID_CHARS = "@_.-+%!#$&*/<=>?'^|~`\\"

type Lexer struct {
	reader *bufio.Reader
	sweet  bool

	aliases map[string]*Token

	line_num int
	finished bool
	comment  int
	mlc_line int

	// private to scanRaw, the current scanned line
	_curline *LexLine

	// private to scanSweet, forms a chain of active lines
	_active *LexLine

	// private to scanSweet, stores results of sweet processing
	_candy  *LexLine
}

type LexLine struct {
	indent int
	tokens []*Token
	parent *LexLine
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(r io.Reader) *Lexer {
	lexer := new(Lexer)

	lexer.reader = bufio.NewReader(r)
	lexer.aliases = make(map[string]*Token)
	lexer._candy = newLexLine()

	return lexer
}

// AddAlias adds a new alias for an identifier.  During Scan(), a
// non-scoped identifier matching name will be replaced with a token
// referring to the new name and module.  AddAlias returns true if
// successful, false if the original name already exists.
func (lex *Lexer) AddAlias(name, new_name, new_module string) bool {
	if lex.aliases[name] != nil {
		return false
	}

	t := new(Token)
	t.Kind = TOK_Name
	t.Str = new_name
	t.Module = new_module

	lex.aliases[name] = t
	return true
}

// Scan returns the next high-level token from the file.
// If the file reaches EOF, or has reached it before, then a
// TOK_EOF token is returned.
//
// Any problems scanning the file will return a TOK_ERROR token,
// which covers both I/O errors and malformed text errors.
func (lex *Lexer) Scan() *Token {
	var t *Token
	if lex.sweet {
		t = lex.scanSweet()
	} else {
		t = lex.scanNormal()
	}

	if t.Kind == TOK_Module {
		msg := "unexpected module name: " + t.Str
		return lex.newTokenAt(TOK_ERROR, msg, t.LineNum)
	}

	// a close bracket at top-level?
	if t.Match(")") || t.Match("]") || t.Match("}") {
		msg := "stray '" + t.Str + "' found (no previous opener)"
		return lex.newTokenAt(TOK_ERROR, msg, t.LineNum)
	}

	// check for non-terminated multi-line comments
	if t.Kind == TOK_EOF && lex.comment > 0 {
		lex.comment = 0
		msg := "unterminated multi-line comment"
		// for this error, use line number where comment began
		return lex.newTokenAt(TOK_ERROR, msg, lex.mlc_line)
	}

	// handle aliases (identifiers which implicitly refer to a module)
	if t.Kind == TOK_Name && t.Module == "" && lex.aliases[t.Str] != nil {
		alias := lex.aliases[t.Str]

		t.Str = alias.Str
		t.Module = alias.Module
	}

	return t
}

// DumpRawTokens is a debugging aid, displays each low-level token.
func (lex *Lexer) DumpRawTokens() {
	for {
		t := lex.scanRaw()

		Print("line %4d: %s\n", t.LineNum, t.String())

		if t.Kind == TOK_EOF {
			break
		}
	}
}

//----------------------------------------------------------------------

func (lex *Lexer) scanNormal() *Token {
	var t *Token

	t = lex.scanRaw()

	if t.Match("\\") || t.Match("=") {
		msg := "cannot use '" + t.Str + "' in that context"
		return lex.newTokenAt(TOK_ERROR, msg, t.LineNum)
	}

	// handle a scoped identifier (module name + normal name)
	if t.Kind == TOK_Module {
		return lex.scanScopedIdentifier(t)
	}

	return lex.scanNormal2(t)
}

func (lex *Lexer) scanNormal2(t *Token) *Token {
	if t.Match("(") {
		return lex.scanCompound(TOK_Expr, "(", ")")
	}
	if t.Match("[") {
		return lex.scanCompound(TOK_Access, "[", "]")
	}
	if t.Match("{") {
		return lex.scanCompound(TOK_Data, "{", "}")
	}

	return t
}

func (lex *Lexer) scanScopedIdentifier(mod *Token) *Token {
	t := lex.scanRaw()

	if t.Kind == TOK_EOF {
		return mod
	}

	// return error immediately (ignore previous token)
	if t.Kind == TOK_ERROR {
		return t
	}

	if t.Kind == TOK_Name && t.Module != "" {
		msg := "too many module scopes before '" + t.Str + "'"
		return lex.newTokenAt(TOK_ERROR, msg, mod.LineNum)
	}

	if !t.Scopable() {
		msg := "unexpected module name: " + mod.Str
		return lex.newTokenAt(TOK_ERROR, msg, mod.LineNum)
	}

	// Ok
	t.Module = mod.Str
	return t
}

func (lex *Lexer) scanCompound(kind TokenKind, opener, closer string) *Token {
	expr := lex.newToken(kind, "")
	expr.Children = make([]*Token, 0)

	for {
		t := lex.scanNormal()

		if t.Kind == TOK_ERROR {
			return t
		}

		// found the closing bracket?
		if t.Match(closer) {
			return expr
		}

		// the REPL requires treating EOF with a different error
		// message, otherwise (def a ]) is never "completeable".

		if t.Kind == TOK_EOF {
			msg := "unterminated expr in " + opener + closer
			// for this error, use line number of opening bracket
			return lex.newTokenAt(TOK_ERROR, msg, expr.LineNum)
		}

		if t.Match(")") || t.Match("]") || t.Match("}") {
			msg := "mismatched brackets: wanted '" + closer + "', got '" + t.Str + "'"
			// for this error, use line number of opening bracket
			return lex.newTokenAt(TOK_ERROR, msg, expr.LineNum)
		}

		expr.Add(t)
	}
}

func (lex *Lexer) scanSweet() *Token {
	for {
		// drain any pending tokens
		if len(lex._candy.tokens) > 0 {
			return lex._candy.popFront()
		}

		// this returned line always has at least one token
		line, err_tok := lex.scanSweetLine()
		if err_tok != nil {
			if err_tok.Kind == TOK_EOF {
				break
			}
			return err_tok
		}

		for lex._active != nil {
			// check for prongs, like the "else" keyword
			force_child := false

			if line.isProng() {
				if line.indent == lex._active.indent {
					force_child = true
				} else if line.indent > lex._active.indent {
					msg := "found stray '" + line.tokens[0].Str + "' prong without a parent"
					return lex.newToken(TOK_ERROR, msg)
				}
			}

			if line.indent > lex._active.indent || force_child {
				// line is a child, push it on stack and continue
				err_tok := lex.pushActiveLine(line)
				if err_tok != nil {
					return err_tok
				}
				goto next_line

			} else {
				// line is not a child, so it terminates a previous
				// line -- pop that off stack and process it.
				err_tok = lex.closeActiveLine(lex._active)
				if err_tok != nil {
					return err_tok
				}

				lex._active = lex._active.parent
			}
		}

		if line.indent > 0 {
			msg := "found indented line without a parent"
			return lex.newToken(TOK_ERROR, msg)
		}

		if line.isProng() {
			msg := "found stray '" + line.tokens[0].Str + "' prong without a parent"
			return lex.newToken(TOK_ERROR, msg)
		}

		{ // ugh, you suck go
		err_tok := lex.pushActiveLine(line)
		if err_tok != nil {
			return err_tok
		} }

		next_line:
	}

	// the end of file terminates all active lines
	for lex._active != nil {
		err_tok := lex.closeActiveLine(lex._active)
		if err_tok != nil {
			return err_tok
		}
		lex._active = lex._active.parent
	}

	if len(lex._candy.tokens) > 0 {
		return lex._candy.popFront()
	} else {
		return lex.newToken(TOK_EOF, "")
	}
}

func (lex *Lexer) pushActiveLine(line *LexLine) *Token {
	// NOTE: only returns a token for an error

	// handle splitting a line at `=` and `=>` tokens.
	// they must not be the very last thing on a line.
	// the `\` symbol at the start inhibits this splitting.
	split_pos := -1
	is_equals := false

	for i, t := range line.tokens {
		if t.Match("\\") {
			break
		}
		if t.Match("=") || t.Match("=>") {
			split_pos = i
			is_equals = t.Match("=")
			break
		}
	}

	if split_pos >= 0 && split_pos < len(line.tokens)-1 {
		// create a "pseudo-line" for front section
		front := newLexLine()
		front.indent = line.indent

		for i := 0; i < split_pos+1; i++ {
			front.addRaw(line.popFront())
		}

		err_tok := lex.pushActiveLine(front)
		if err_tok != nil {
			return err_tok
		}

		split_pos = -1
	}

	// handle `=` in assignment and let forms
	if split_pos >= 0 && is_equals {
		eq := line.tokens[split_pos]

		if split_pos == 1 {
			eq.Str = "set!"
			line.tokens[1] = line.tokens[0]
			line.tokens[0] = eq

		} else if split_pos == 2 &&
			(line.tokens[0].Match("let") ||
			 line.tokens[0].Match("let-ref")) {

			line.tokens = line.tokens[0:2]

		} else if split_pos == 3 &&
			 (line.tokens[0].Match("var")) {

			line.tokens = line.tokens[0:3]

		} else {
			msg := "invalid use of '=' assignment syntax"
			return lex.newTokenAt(TOK_ERROR, msg, line.tokens[0].LineNum)
		}
	}

	line.parent = lex._active
	lex._active = line

	return nil
}

func (lex *Lexer) closeActiveLine(line *LexLine) *Token {
	// NOTE: only returns a token for an error

	dest := line.parent
	if dest == nil {
		dest = lex._candy
	}

	// prongs like "elif" and "else" need to inhibit S-Exprs
	force_plain := line.isProng()
	force_s_exp := false

	// also these two keywords (used in match expressions)
	if line.tokens[0].Match("where") || line.tokens[0].Match("=>") {
		force_plain = true
	}

	// handle `\` symbol which inhibits making an S-Expr
	// TODO: review this, is it useful at all?
	if line.tokens[0].Match("\\") {
		if len(line.tokens) == 1 {
			msg := "no tokens after the '\\' symbol"
			return lex.newTokenAt(TOK_ERROR, msg, line.tokens[0].LineNum)
		}
		line.popFront()
		force_plain = true
	}

	for _, bks := range line.tokens {
		if bks.Match("\\") {
			msg := "cannot use '" + bks.Str + "' in that context"
			return lex.newTokenAt(TOK_ERROR, msg, bks.LineNum)
		}
	}

	// a few keywords often occur alone, but need to be S-exprs
	if len(line.tokens) == 1 {
		head := line.tokens[0]

		if head.Match("return") || head.Match("break") || head.Match("continue") {
			force_s_exp = true
		}
	}

	if force_plain || (len(line.tokens) == 1 && !force_s_exp) {
		for _, t := range line.tokens {
			dest.addRaw(t)
		}

	} else {
		t := lex.newTokenAt(TOK_Expr, "", line.tokens[0].LineNum)
		t.Children = line.tokens
		line.tokens = nil

		dest.addRaw(t)
	}

	return nil
}

func (lex *Lexer) scanSweetLine() (*LexLine, *Token) {
	line := newLexLine()

	// loop in order to skip blank lines
	for {
		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			return nil, err_tok
		}

		if !lex.scanLine(s, line) {
			return nil, line.tokens[0]
		}

		if len(line.tokens) > 0 {
			break
		}

		// has a multi-line comment begun, with nothing before it?
		// in this case we need to remember the indentation level,
		// and restore it when it finishes, since each call to
		// scanLine() while active will clear it to zero.
		if lex.comment > 0 {
			save_indent := line.indent

			for lex.comment > 0 {
				s, err_tok := lex.fetchRawLine()
				if err_tok != nil {
					return nil, err_tok
				}
				if !lex.scanLine(s, line) {
					return nil, line.tokens[0]
				}
			}

			line.indent = save_indent

			if len(line.tokens) > 0 {
				break
			}
		}
	}

	// iterate over line to handle bracketed forms.
	// this can potentially fetch extra lines and decode them
	// via the scanNormal() code.

	line2 := newLexLine()
	line2.indent = line.indent

	for len(line.tokens) > 0 {
		t := line.popFront()

		if t.Match(")") || t.Match("]") || t.Match("}") {
			msg := "stray '" + t.Str + "' found (no previous opener)"
			return nil, lex.newTokenAt(TOK_ERROR, msg, t.LineNum)
		}

		if t.Match("(") || t.Match("[") || t.Match("{") {
			// force scanRaw to access the remaining tokens.
			// [ luckily we cannot recurse into here, so this
			//   simplistic and somewhat hacky method is OK ]
			lex._curline = line

			t = lex.scanNormal2(t)
			if t.IsError() {
				return nil, t
			}

			line = lex._curline
			lex._curline = nil
		}

		// handle `()` after an identifier, create a function call
		if line2.trySweetFuncCall(t) {
			// ok
		} else {
			line2.add(t)
		}
	}

	if line2.hasError() {
		return nil, line2.tokens[0]
	} else {
		return line2, nil
	}
}

// scanRaw scans the file and returns the next low-level token.
// If the file reaches EOF, or has reached it before, then a TOK_EOF
// token is returned.
//
// Any problems scanning the file will return a TOK_ERROR token,
// which covers both I/O errors and malformed text errors.
func (lex *Lexer) scanRaw() *Token {
	for {
		if lex._curline == nil {
			lex._curline = newLexLine()
		}

		if len(lex._curline.tokens) > 0 {
			return lex._curline.popFront()
		}

		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			return err_tok
		}

		lex.scanLine(s, lex._curline)
	}
}

func (lex *Lexer) fetchRawLine() (string, *Token) {
	// once stream is finished, keep returning EOF
	// [ this simplifies some logic in the calling code ]
	if lex.finished {
		return "", lex.newToken(TOK_EOF, "")
	}

	// need to scan the next line
	lex.line_num += 1

	// NOTE: this can return some data + io.EOF
	s, err := lex.reader.ReadString('\n')

	if err == io.EOF {
		lex.finished = true
	} else if err != nil {
		lex.finished = true
		return "", lex.newToken(TOK_ERROR, err.Error())
	}

	// strip CR and LF from the end
	if len(s) > 0 && s[len(s)-1] == '\n' {
		s = s[0:len(s)-1]
	}
	if len(s) > 0 && s[len(s)-1] == '\r' {
		s = s[0:len(s)-1]
	}

	return s, nil
}

func (lex *Lexer) scanLine(s string, line *LexLine) bool {
	line.clear()

	// convert line to an array of runes
	runes := []rune(s)

	// check for errors
	for _, ch := range runes {
		if ch == utf8.RuneError {
			line.add(lex.newToken(TOK_ERROR, "bad utf8 in file"))
			return false
		}
	}

	if lex.sweet {
		runes = lex.countTabs(runes, line)
		if runes == nil {
			return false
		}
	}

	r := runes

	for len(r) > 0 {
		// multi-line comment?
		if r[0] == ';' && len(r) >= 2 {
			if r[1] == '[' {
				if lex.comment == 0 {
					lex.mlc_line = lex.line_num
				}
				lex.comment += 1
				r = r[2:]
				continue

			} else if r[1] == ']' {
				lex.comment -= 1

				if lex.comment < 0 {
					msg := "stray ';]' found (no previous opener)"
					line.add(lex.newToken(TOK_ERROR, msg))
					return false

				} else if lex.comment == 0 {
					r = r[2:]
					continue
				}
			}
		}

		// while inside multi-line comments, nothing else is significant
		if lex.comment > 0 {
			r = r[1:]
			continue
		}

		// whitespace ?
		if unicode.Is(unicode.White_Space, r[0]) ||
			unicode.IsControl(r[0]) {
			r = r[1:]
			continue
		}

		// comment ?
		if r[0] == ';' {
			break
		}

		// string ?
		if r[0] == '"' {
			size, tok := lex.decodeString(r)

			line.add(tok)
			if line.hasError() {
				return false
			}
			r = r[size:]
			continue
		}

		// character literal ?
		if r[0] == '\'' {
			size, tok := lex.decodeCharacter(r)

			line.add(tok)
			if line.hasError() {
				return false
			}
			r = r[size:]
			continue
		}

		// number ?
		if unicode.IsDigit(r[0]) ||
			(r[0] == '-' && len(r) >= 2 && unicode.IsDigit(r[1])) {

			size, tok := lex.decodeNumber(r)

			line.add(tok)
			if line.hasError() {
				return false
			}
			r = r[size:]
			continue
		}

		// quoted identifier?
		if r[0] == '`' {
			size, tok := lex.decodeQuotedIdent(r)

			line.add(tok)
			if line.hasError() {
				return false
			}
			r = r[size:]
			continue
		}

		// anything else MUST be a name / symbol
		size, tok := lex.decodeIdentifier(r)

		line.add(tok)
		if line.hasError() {
			return false
		}
		r = r[size:]
	}

	return true
}

func (lex *Lexer) countTabs(r []rune, line *LexLine) []rune {
	// multi-line comments are free-form, do not force tabs in them
	if lex.comment > 0 {
		return r
	}

	visible_tab := false

	for len(r) > 0 {
		switch {
		case r[0] == '\t':
			if visible_tab {
				msg := "cannot use invisible tabs after a visible tab"
				line.add(lex.newToken(TOK_ERROR, msg))
				return nil
			}
			line.indent += 1
			r = r[1:]

		case r[0] == '|':
			visible_tab = true
			line.indent += 1
			r = r[1:]

			// require a space, except at end of a line
			if len(r) > 0 && r[0] != ' ' {
				msg := "visible tab must have following spaces"
				line.add(lex.newToken(TOK_ERROR, msg))
				return nil
			}

			for len(r) > 0 && r[0] == ' ' {
				r = r[1:]
			}

		case r[0] < 32:
			return r

		case unicode.Is(unicode.White_Space, r[0]):
			msg := "tabs must be used for indentation, not spaces"
			line.add(lex.newToken(TOK_ERROR, msg))
			return nil

		default:
			return r
		}
	}

	return r
}

func (lex *Lexer) decodeString(r []rune) (int, *Token) {
	// skip leading quote
	pos := 1

	s := ""

	for {
		if pos >= len(r) {
			return -1, lex.newToken(TOK_ERROR, "unterminated string")
		}

		if r[pos] == '"' {
			break
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) {
			pos += 1

			ch, step := lex.decodeEscape(r[pos:])
			if step == -2 {
				return -1, lex.newToken(TOK_ERROR, "unknown escape in string")
			} else if step < 0 {
				return -1, lex.newToken(TOK_ERROR, "malformed escape in string")
			}

			s = s + string(ch)
			pos += step
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}

	return pos + 1, lex.newToken(TOK_String, s)
}

func (lex *Lexer) decodeCharacter(r []rune) (int, *Token) {
	// skip leading quote
	pos := 1

	if len(r) < 3 || r[1] == '\'' {
		return -1, lex.newToken(TOK_ERROR, "bad character literal")
	}

	if r[2] == '\'' && r[1] != '\\' {
		return 3, lex.newToken(TOK_Char, string(r[1]))
	}

	// handle escapes
	if r[pos] != '\\' {
		return -1, lex.newToken(TOK_ERROR, "bad character literal")
	}

	pos += 1
	r = r[pos:]

	ch, step := lex.decodeEscape(r)
	if step == -2 {
		return -1, lex.newToken(TOK_ERROR, "unknown escape in char literal")
	} else if step < 0 {
		return -1, lex.newToken(TOK_ERROR, "malformed escape in char literal")
	}

	r = r[step:]

	if len(r) < 1 || r[0] != '\'' {
		return -1, lex.newToken(TOK_ERROR, "unterminated char literal")
	}

	step += pos + 1

	return step, lex.newToken(TOK_Char, string(ch))
}

func (lex *Lexer) decodeEscape(r []rune) (rune, int) {
	switch r[0] {
	case '"':
		return '"', 1
	case '\'':
		return '\'', 1
	case '\\':
		return '\\', 1
	case 'a':
		return '\a', 1
	case 'b':
		return '\b', 1
	case 'f':
		return '\f', 1
	case 'n':
		return '\n', 1
	case 'r':
		return '\r', 1
	case 't':
		return '\t', 1
	case 'v':
		return '\v', 1
	}

	// hexadecimal?
	// (requires two hexadecimal digits, no more, no less)
	if r[0] == 'x' {
		return lex.decodeHexEscape(r, 2)
	}

	// unicode?
	// (these follow conventions of C11 and Go)
	if r[0] == 'u' {
		return lex.decodeHexEscape(r, 4)
	}
	if r[0] == 'U' {
		return lex.decodeHexEscape(r, 8)
	}

	// octal?
	// (requires three octal digits, no more, no less)
	if '0' <= r[0] && r[0] <= '3' {
		if len(r) >= 3 &&
			'0' <= r[1] && r[1] <= '7' &&
			'0' <= r[2] && r[2] <= '7' {
			a := r[0] - '0'
			b := r[1] - '0'
			c := r[2] - '0'
			return a*64 + b*8 + c, 3
		}

		return utf8.RuneError, -1
	}

	return utf8.RuneError, -2
}

func (lex *Lexer) decodeHexEscape(r []rune, size int) (rune, int) {
	// this assumes first character is part of the escape (the 'x' or 'u')

	if len(r) < size+1 {
		return utf8.RuneError, -1
	}

	var result rune

	for i := 1; i <= size; i++ {
		result = result << 4
		ch := unicode.ToUpper(r[i])
		if '0' <= ch && ch <= '9' {
			result |= (ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			result |= 10 + (ch - 'A')
		} else {
			return utf8.RuneError, -1
		}
	}

	return result, size + 1
}

// returns a token and # of runes consumed
func (lex *Lexer) decodeNumber(r []rune) (int, *Token) {
	pos := 1

	format_hex := false
	format_bin := false

	if (len(r) >= 3 && r[0] == '0' && r[1] == 'x') ||
		(len(r) >= 4 && r[0] == '-' && r[1] == '0' && r[2] == 'x') {
		format_hex = true
		pos = 2
		if r[0] == '-' {
			pos = 3
		}
	}
	if (len(r) >= 3 && r[0] == '0' && r[1] == 'b') ||
		(len(r) >= 4 && r[0] == '-' && r[1] == '0' && r[2] == 'b') {
		format_bin = true
		pos = 2
		if r[0] == '-' {
			pos = 3
		}
	}

	seen_dot := false
	seen_exp := false

	for pos < len(r) {
		// check for an exponent (floating point)
		if !format_hex && !format_bin && (r[pos] == 'e' || r[pos] == 'E') {
			if seen_exp {
				return -1, lex.newToken(TOK_ERROR, "bad float: too many exponents")
			}
			pos += 1
			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				pos += 1
			}
			if !(pos < len(r) && unicode.IsDigit(r[pos])) {
				return -1, lex.newToken(TOK_ERROR, "bad float: no digit after e")
			}
			pos += 1
			seen_exp = true
			continue
		}

		// floating point syntax
		if r[pos] == '.' {
			if format_hex {
				return -1, lex.newToken(TOK_ERROR, "bad hex number")
			}
			if format_bin {
				return -1, lex.newToken(TOK_ERROR, "bad binary number")
			}
			if seen_dot {
				return -1, lex.newToken(TOK_ERROR, "bad float: too many periods")
			}
			pos += 1
			if !(pos < len(r) && unicode.IsDigit(r[pos])) {
				return -1, lex.newToken(TOK_ERROR, "bad float: no digit after period")
			}
			pos += 1
			seen_dot = true
			continue
		}

		// check for end-of-number, and also invalid digits
		if unicode.IsLetter(r[pos]) {
			if format_hex {
				if !unicode.Is(unicode.Hex_Digit, r[pos]) {
					return -1, lex.newToken(TOK_ERROR, "illegal hex digit")
				}
			} else {
				return -1, lex.newToken(TOK_ERROR, "bad number: contains letters")
			}

			pos += 1
			continue

		} else if unicode.IsDigit(r[pos]) {
			if format_bin && !(r[pos] == '0' || r[pos] == '1') {
				return -1, lex.newToken(TOK_ERROR, "illegal binary digit")
			}

			pos += 1
			continue
		}

		break
	}

	kind := TOK_Int
	if seen_dot || seen_exp {
		kind = TOK_Float
	}

	return pos, lex.newToken(kind, string(r[0:pos]))
}

func (lex *Lexer) decodeQuotedIdent(r []rune) (int, *Token) {
	// skip leading quote
	pos := 1

	s := ""

	for {
		if pos >= len(r) {
			return -1, lex.newToken(TOK_ERROR, "unterminated quoted identifier")
		}

		if r[pos] == '`' {
			break
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) && r[pos+1] == '\\' {
			s = s + "\\"
			pos += 2
			continue
		}
		if r[pos] == '\\' && pos+1 < len(r) && r[pos+1] == '`' {
			s = s + "`"
			pos += 2
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}

	if len(s) == 0 {
		msg := "quoted identifier cannot be empty"
		return -1, lex.newToken(TOK_ERROR, msg)
	}

	res := lex.newToken(TOK_Name, s)
	res.Quoted = true

	return pos + 1, res
}

// returns # of runes consumed, or negative on error
func (lex *Lexer) decodeIdentifier(r []rune) (int, *Token) {
	pos := 0

	// handle '::' symbol
	if len(r) >= 2 && r[0] == ':' && r[1] == ':' {
		return 2, lex.newToken(TOK_Name, string(r[0:2]))
	}

	// handle non-ident symbols
	if strings.ContainsRune("()[]{}:", r[0]) {
		return 1, lex.newToken(TOK_Name, string(r[0:1]))
	}

	for pos < len(r) {
		if !LEX_IsIdentChar(r[pos]) {
			break
		}

		pos += 1
	}

	// tell caller we could not parse an identifier
	if pos == 0 {
		msg := "illegal character '" + string(r[0]) + "'"
		return -1, lex.newToken(TOK_ERROR, msg)
	}

	s := string(r[0:pos])

	return pos, lex.newToken(TOK_Name, s)
}

func (lex *Lexer) newToken(kind TokenKind, str string) *Token {
	t := new(Token)
	t.Kind = kind
	t.Str = str
	t.LineNum = lex.line_num
	return t
}

func (lex *Lexer) newTokenAt(kind TokenKind, str string, line int) *Token {
	t := new(Token)
	t.Kind = kind
	t.Str = str
	t.LineNum = line
	return t
}

//----------------------------------------------------------------------

func newLexLine() *LexLine {
	ln := new(LexLine)
	ln.tokens = make([]*Token, 0)
	return ln
}

func (ln *LexLine) clear() {
	ln.indent = 0
	ln.tokens = ln.tokens[0:0]
}

func (ln *LexLine) add(t *Token) {
	// skip everything after an error
	if ln.hasError() {
		return
	}

	// errors will "eat" all previous tokens on a line
	if t.Kind == TOK_ERROR {
		ln.tokens = ln.tokens[0:0]
	}

	// scope identifiers which follow a module name
	if len(ln.tokens) > 0 &&
		ln.tokens[len(ln.tokens)-1].Kind == TOK_Module {

		mod_name := ln.tokens[len(ln.tokens)-1].Str

		if t.Kind == TOK_Name && t.Module != "" {
			t.Kind = TOK_ERROR
			t.Str = "too many module scopes before '" + t.Str + "'"
			ln.add(t)
			return
		}

		if !t.Scopable() {
			t.Kind = TOK_ERROR
			t.Str = "unexpected module name: " + mod_name
			ln.add(t)
			return
		}

		t.Module = mod_name

		// replace the module token
		ln.tokens[len(ln.tokens)-1] = t
		return
	}

	ln.tokens = append(ln.tokens, t)
}

func (ln *LexLine) addRaw(t *Token) {
	ln.tokens = append(ln.tokens, t)
}

func (ln *LexLine) popFront() *Token {
	res := ln.tokens[0]
	ln.tokens = ln.tokens[1:]
	return res
}

func (ln *LexLine) hasError() bool {
	if len(ln.tokens) == 0 {
		return false
	} else {
		return (ln.tokens[0].Kind == TOK_ERROR)
	}
}

func (ln *LexLine) isProng() bool {
	if len(ln.tokens) == 0 {
		return false
	} else {
		return ln.tokens[0].IsProng()
	}
}

func (ln *LexLine) trySweetFuncCall(t *Token) bool {
	if !(t.Kind == TOK_Expr && len(t.Children) == 0) {
		return false
	}

	if len(ln.tokens) == 0 {
		return false
	}

	if len(ln.tokens) >= 2 {
		prev := ln.tokens[len(ln.tokens)-2]
		if prev.Match("fun") || prev.Match("extern-fun") {
			return false
		}
	}

	last := ln.tokens[len(ln.tokens)-1]
	if last.Kind != TOK_Name {
		return false
	}
	if last.Match("lam") || last.Match("rule") {
		return false
	}

	t.Add(last)

	ln.tokens[len(ln.tokens)-1] = t
	return true
}

func LEX_IsIdentifier(s string) bool {
	if len(s) == 0 {
		return false
	}

	r := []rune(s)

	// number ?
	if r[0] == '-' || unicode.IsDigit(r[0]) {
		return false
	}

	for _, ch := range r {
		if !LEX_IsIdentChar(ch) {
			return false
		}
	}

	return true
}

func LEX_IsIdentChar(ch rune) bool {
	return false ||
		unicode.IsLetter(ch) ||
		unicode.IsDigit(ch) ||
		strings.ContainsRune(ID_CHARS, ch)
}

//----------------------------------------------------------------------

func (t *Token) Add(child *Token) {
	t.Children = append(t.Children, child)
}

func (t *Token) Prepend(child *Token) {
	t.Children = append(t.Children, nil)
	copy(t.Children[1:], t.Children[0:])
	t.Children[0] = child
}

// Match is a convenience method for checking that the token is a
// TOK_Name matching the given string.  Since this is a main way
// to detect language keywords, quoted identifiers always fail.
func (t *Token) Match(name string) bool {
	return (t.Str == name && t.Module == "" && !t.Quoted) &&
		(t.Kind == TOK_Name || t.Kind == ND_Global ||
		t.Kind == ND_Local)
}

func (t *Token) IsError() bool {
	return t.Kind == TOK_ERROR
}

func (t *Token) IsField() bool {
	return t.Kind == TOK_Name && len(t.Str) >= 2 && t.Str[0] == '.'
}

func (t *Token) IsNamedField() bool {
	if !t.IsField() {
		return false
	}

	runes := []rune(t.Str)
	ch := runes[1]

	return unicode.IsLetter(ch) || ch == '_'
}

func (t *Token) IsTag() bool {
	return t.Kind == TOK_Name && len(t.Str) >= 2 && t.Str[0] == '`'
}

func (t *Token) IsGeneric() bool {
	return t.Kind == TOK_Name && len(t.Str) >= 2 && t.Str[0] == '@'
}

func (t *Token) IsProng() bool {
	if t.Match("elif") || t.Match("else") {
		return true
	}
	return t.Kind == TOK_Name && len(t.Str) >= 2 && t.Str[0] == '^'
}

func (t *Token) IsEllipsis() bool {
	return t.Kind == TOK_Name && len(t.Str) >= 3 &&
		t.Str[len(t.Str)-3:len(t.Str)] == "..."
}

func (t *Token) Scopable() bool {
	if t.Kind != TOK_Name {
		return false
	}

	// already scoped?
	if t.Module != "" {
		return false
	}

	// special symbols must never be scoped
	if t.Match(":") || t.Match("::") ||
		t.Match("(") || t.Match("[") || t.Match("{") ||
		t.Match(")") || t.Match("]") || t.Match("}") {
		return false
	}

	return true
}

func (t *Token) Replace(other *Token) {
	t.Kind = other.Kind
	t.Info = other.Info
	t.Str = other.Str
	t.Module = other.Module
	t.Children = other.Children
}

func (t *Token) Clone(deep bool) *Token {
	t2 := new(Token)

	t2.Kind = t.Kind
	t2.Str = t.Str
	t2.LineNum = t.LineNum

	if len(t.Children) > 0 {
		t2.Children = make([]*Token, 0)

		for _, child := range t.Children {
			if deep {
				t2.Add(child.Clone(true))
			} else {
				t2.Add(child)
			}
		}
	}

	return t2
}

func (t *Token) String() string {
	if t == nil {
		return "nil"
	}

	switch t.Kind {
	case TOK_EOF:
		return "EOF"
	case TOK_ERROR:
		return "ERROR '" + t.Str + "'"

	case TOK_Int:
		return "Int '" + t.Str + "'"
	case TOK_Float:
		return "Float '" + t.Str + "'"
	case TOK_String:
		return "String '" + t.Str + "'"
	case TOK_Char:
		return "Char '" + t.Str + "'"

	case TOK_Name:
		return "Name '" + t.Module + t.Str + "'"
	case TOK_Module:
		return "Module '" + t.Str + "'"

	case TOK_Expr:
		return fmt.Sprintf("Expr (%d elem)", len(t.Children))
	case TOK_Access:
		return fmt.Sprintf("Access [%d elem]", len(t.Children))
	case TOK_Data:
		return fmt.Sprintf("Data {%d elem}", len(t.Children))

	// invoke code in parse.go for the ND_XXX kinds
	default:
		return t.String2()
	}
}
