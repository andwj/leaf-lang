// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"
// import "strings"
// import "math"

type GlobalDef struct {
	name    string // its name

	is_fun  bool   // declared via (fun ...) instead of (var ...)
	mutable bool   // can be altered?
	extern  bool   // defined externally?
	private bool   // local to just this file?
	zeroed  bool   // place into the BSS segment?
	failed  bool   // parsing/deducing/compiling has failed?

	ty      *Type  // its type (as specified or deduced)
	ref_ty  *Type  // the TYP_ref version (for in-code usage)
	fun_cl  *Closure // info for functions

	tree    *Token // parse tree (before and after ParseElement)
}

var visibility_private bool

type Closure struct {
	// true if this is an expression (not a function)
	is_expr bool

	// token for the body of the function, or the expression
	t_body *Token

	// original parameter tokens, inside a TOK_Expr.
	t_parameters *Token

	// the static type of this function (once fully compiled).
	// it also contains the parameter names.
	ty *Type

	// the return type of the function or expression.
	// normally this mirrors ty.sub -- however this may be set
	// earlier when return type is known but not the parameters.
	ret_type *Type

	// the variables which are the parameters of the function.
	parameters []*LocalVar

	debug_name string

	// the total number of used stack slots (below the saved RBP value),
	// not including the saved RBP value.
	stack_len int

	// did the function or method fail to parse/compile?
	failed bool
}

func NewClosure(debug_name string) *Closure {
	cl := new(Closure)
	cl.debug_name = debug_name
	cl.parameters = make([]*LocalVar, 0)

	return cl
}

//----------------------------------------------------------------------

const (
	GLOB_FUN int = 1 << iota
	GLOB_EXTERN
	GLOB_ZEROED
)

func Glob_CreateRaw(t *Token, flags int) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad global def: missing name")
		return FAILED
	}

	t_name := children[1]
	if t_name.Kind != TOK_Name {
		PostError("bad global def: expected name got %s",
			t_name.String())
		return FAILED
	}

	// global defs cannot be redefined
	if context.HasDef(t_name.Str) {
		PostError("global '%s' is already defined", t_name.Str)
		return FAILED
	}

	gdef := new(GlobalDef)
	gdef.name = t_name.Str

	context.AddDef(gdef.name, gdef)

	gdef.is_fun  = ((flags & GLOB_FUN) != 0)
	gdef.extern  = ((flags & GLOB_EXTERN) != 0)
	gdef.zeroed  = ((flags & GLOB_ZEROED) != 0)
	gdef.private = visibility_private
	gdef.mutable = !gdef.is_fun

	gdef.tree = t

	return OKAY
}

func Glob_ParseDef(gdef *GlobalDef) {
	t := gdef.tree
	if t == nil {
		return
	}

	var err2 cmError

	t, err2 = ParseMainElement(t)
	if err2 != OKAY {
		return
	}

	switch t.Kind {
	case ND_DefVar:
		glob_HandleVar(gdef, t)

	case ND_DefFunc:
		glob_HandleFunc(gdef, t)

	case ND_ExternVar, ND_ZeroedVar:
		glob_HandleExternalVar(gdef, t)

	case ND_ExternFunc:
		glob_HandleExternalFunc(gdef, t)

	default:
		panic("weird node from ParseElement of a global")
	}
}

func glob_HandleVar(gdef *GlobalDef, t *Token) cmError {
	gdef.tree = t.Children[1]

	gdef.ty = t.Info.ty
	gdef.ref_ty = gdef.ty.MakeRefType()

	return OKAY
}

func glob_HandleFunc(gdef *GlobalDef, t *Token) cmError {
	t_name := t.Children[0]
	t_pars := t.Children[1]
	t_body := t.Children[2]

	debug_name := t_name.Module + t_name.Str

	cl := CreateClosureStuff(t, t_pars, t_body, debug_name)
	cl.ty = t.Info.ty

	gdef.ty = t.Info.ty
	gdef.ref_ty = gdef.ty.MakeRefType()
	gdef.fun_cl = cl

	return OKAY
}

func CreateClosureStuff(t, t_pars, t_body *Token, debug_name string) *Closure {
	cl := NewClosure(debug_name)

	cl.t_body = t_body
	cl.t_parameters = t_pars
	cl.ret_type = t.Info.ty.sub

	return cl
}

func glob_HandleExternalVar(gdef *GlobalDef, t *Token) cmError {
	gdef.ty = t.Info.ty
	gdef.ref_ty = gdef.ty.MakeRefType()

	return OKAY
}

func glob_HandleExternalFunc(gdef *GlobalDef, t *Token) cmError {
	gdef.ty = t.Info.ty
	gdef.ref_ty = gdef.ty.MakeRefType()

	return OKAY
}

//----------------------------------------------------------------------

func Glob_BindNames(gdef *GlobalDef) {
	if gdef.extern {
		return
	}

	t := gdef.tree
	if t == nil {
		return
	}

	if gdef.is_fun {
		// function
		if BindClosure(gdef.fun_cl) != OKAY {
			gdef.failed = true
			gdef.fun_cl.failed = true
		}
	} else {
		// variable
		if BindDataExpr(gdef.tree) != OKAY {
			gdef.failed = true
		}
	}
}

//----------------------------------------------------------------------

func Glob_TypeCheck(gdef *GlobalDef) {
	if gdef.zeroed || gdef.extern || gdef.tree == nil {
		return
	}

	if gdef.is_fun {
		// function
		if DeduceClosure(gdef.fun_cl) != OKAY {
			gdef.failed = true
			gdef.fun_cl.failed = true
		}
	} else {
		// variable
		if DeduceGlobalVar(gdef) != OKAY {
			gdef.failed = true
		}
	}
}

//----------------------------------------------------------------------

type VN_Flags int

const (
	ALLOW_UNDERSCORE VN_Flags = (1 << iota)
	ALLOW_FIELDS
	ALLOW_SELF
)

func ValidateDefName(t *Token, what string, flags VN_Flags) cmError {
	if t.Module != "" {
		PostError("cannot define a %s in another module", what)
		return FAILED
	}

	name := t.Str

	if t.Str == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OKAY
	}

	// disallow language keywords
	if !t.Quoted && IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow field names
	if len(name) >= 2 && name[0] == '.' {
		if (flags & ALLOW_FIELDS) != 0 {
			return OKAY
		} else {
			PostError("bad %s name: cannot begin with a dot", what)
			return FAILED
		}
	}

	// disallow names of user constants
	if context.HasConst(name) {
		PostError("bad %s name: '%s' already defined as a const", what, name)
		return FAILED
	}

	// disallow names of user types
	if context.HasType(name) {
		PostError("bad %s name: '%s' already defined as a type", what, name)
		return FAILED
	}

	return OKAY
}

func IsLanguageKeyword(name string) bool {
	// TODO review this

	switch name {
	case "#public", "#private":
		return true

	case "type", "alias":
		return true

	case "let", "var", "extern-var", "zeroed-var":
		return true

	case "fun", "extern-fun", "lam":
		return true

	case "begin", "if", "else", "elif", "while":
		return true

	case "cast", "set!":
		return true

	case "skip", "junction", "return", "break", "continue":
		return true

	case "s8", "s16", "s32", "s64":
		return true

	case "u8", "u16", "u32", "u64", "void":
		return true

	case "^", "array", "struct", "union", "function":
		return true

	case "False", "True", "Null":
		return true

	case ":", "::", "_", "->", "=>":
		return true
	}

	return false
}
