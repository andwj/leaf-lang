
The Yew-Leaf Language
=====================

by Andrew Apted, 2020.


Introduction
------------

This language is a fairly low-level language which is planned
to be the target of my high-level language Yewberry, as well as
used to implement most of its runtime and standard library.
It aims to be a nice abstraction over assembly language, but
lacking a lot of the policy and cruft of the C language.


Syntax
------

The overall syntax is like Scheme or LISP, using "S-expressions"
within parentheses.  The "sweet syntax" can be used too, which
uses significant whitespace (tabs to be exact).

Identifiers for new things (vars, functions, types, etc) may be
enclosed in back-quotes, and this guarantees they will never
clash with any keyword of the language.  This is recommended
for code which is generated (e.g. by a high-level compiler).


Files
-----

The ".lf" extension will be used for leaf code files.  The sweet
syntax variant is always enabled, though it is a superset of the
normal (parenthetic) syntax so than can be used too.  The output
of this compiler will be assembly code.

Global variables and functions can be defined in any order, there
is no need for forward declarations in this language.


Types
-----

The basic types are as follows.  The `u` prefix signifies an
unsigned value, and `s` prefix signifies a signed value.

```
u8,  s8  :  8-bit integer, occupies 1 byte
u16, s16 : 16-bit integer, occupies 2 bytes
u32, s32 : 32-bit integer, occupies 4 bytes
u64, s64 : 64-bit integer, occupies 8 bytes

f32 : 32-bit floating point, occupies 4 bytes
f64 : 64-bit floating point, occupies 8 bytes

void : signifies the lack of a value

no-return : signifies the function never returns
```

An important property of the basic types is that they fit into
one of the CPU registers.  Due to this, pointer and reference
types are considered to be "basic" too, unlike complex types
which *always* occupy a piece of memory.

Void types cannot be used in most places where a type is needed,
e.g. variables, parameters and struct fields cannot be void.
One place where it *can* be used for the result of a function.
Also certain language forms, like `set!` and `while` have no
result and hence are implicitly void.

The `no-return` type is useful to specify a function which never
returns, but it is also the type of `skip` statements (and their
convenience forms `return`, `break` and `continue`), as well as
the type of a while loop which can be determined to never end.

The non-numeric types are described below....


Pointers and References
-----------------------

Pointer and reference types are similar, as they both represent
the location of something in memory (the thing that is pointed
to).  References can be thought of as "magical", being deref'd
when needed, and can never be Null.  Pointers can be thought of
as "unsafe", since pointer types can be freely cast to any other
pointer type or even an integer, and may be Null.  Generally a
program should use references as much as possible.

These types take a single sub-type in their definition, which
is the target type (the thing pointed to).  If that type would
normally be in parentheses, those parentheses can be omitted.

```
(ref s64)
(ref (array 40 u8))
(ref MyStruct)

(^ f64)
(^ (struct .x f64 .y f64))
(^ array 200 u8)
```

A pointer value (or a reference to one) can be "dereferenced" by
using the access syntax in `[]` square brackets, with no other
elements.  It produces a reference to the pointed at value, which
can then be used (read or written) like a global variable.

Note that dereferencing a Null pointer is illegal, it will usually
be caught (either by an explicit test in the compiled code or by
implicit test using the MMU) and the program will be terminated or
just crash.

```
(print-int [some-ptr])
```

The square bracket syntax is also used with references to an
array to perform the index operation, or with references to a
struct or union to perform a field access -- see the sections
below.

The `deref` form allows explicitly dereferencing a reference.
This is rarely needed since this happens automatically is most
contexts (like passing to a parameter or assigning as a value).
Using this with a reference to a complex type, such as an array
or struct, is invalid and disallowed.

```
(print-hex (deref my-u64-ref))
```


Array Types
-----------

An array represents a block of elements of a particular type
stored in memory.  Arrays come in two varieties: fixed size and
open (unsized).

Open arrays represent an area of memory where the length is stored
stored somewhere else or determined from the contents.  They are
more unsafe than normal arrays since it is not possible to verify
that an index is valid.  Open arrays do not have a size, and hence
cannot be used in places where a size is required, such as the
element type of another array, or a field in a struct or union.

```
(array 10 u32)

(open-array (^ Foo))
```

An element can be accessed using notation in square brackets.
The first element must be a reference to an array, and the
result is a reference to the indexed element.

```
[some-arr 5]
```


Struct Types
------------

Structure types are a group of heterogenous elements, whose
arrangement and sizes are fixed.  The field names must begin
with a `.` period.  Padding must be explicit, and can be done
using a name which is only a single period.

An "open struct" is one whose final field is an open array or
another open struct.  Such structs have the same restrictions
on usage as open arrays, e.g. they cannot be the element type
of an array.

```
(struct
   .x    f64
   .y    f64
   .type u32
   .     u32
)

(open-struct
   .length u32
   .data   (open-array f64)
)
```

The field of a struct is accessed using square brackets.
The first element must be a reference to a struct, and the
result is a reference to the named element.

```
[my-struct .type]
```


Union Types
-----------

A union type is one where different types can occupy the same
piece of memory.  The size of the union is the same as its
largest element.

```
(union
   .float f64
   .int   s64
   .char  u32
   .      (array 16 u8)
)
```

The field of a union is accessed using square brackets, like
accessing a struct.  The first element must be a reference to a
union, and the result is a reference to the named element.

```
[my-union .float]
```


Function Types
--------------

A function type specifies the types of each parameter and
the result type returned from the function.  The result type
is always last and always present.

When making a function call, the first element in `()` must
be a reference to a function type.

```
(function u32 f64 void)
```


Zero Values
-----------

Most types have a default value which is called a "zero value"
since the representation always consists of all bits and bytes
being zero (clear).  For integers the value is 0, for floating
point the value is 0.0, and for pointers it is Null.

Compound types (arrays, structs and unions) only have a zero
value if all of their components have a zero value, including
padding fields.

Reference types lack a zero value since they can never be Null.
Functions, open arrays and open structs also lack a zero value.
The `void` type is considered to have one, though in practise
there is no place it can actually be used.


Custom Type Names
-----------------

Custom types provide a type with a user-supplied name.  This
is actually the only way to create recursive types, which are
usually legal except when the size would be infinite.

```
(type Blah u64)
(type FooPtr (^ Foo))
(type Thing (struct .x f64 .y f64 .type u64))
```


Type Compatibility
------------------

Two types are considered equal if they have the same underlying
structure.  Names are ignored for this.  Signed integers are
considered to be different to unsigned integers.  References
are distinct from pointers.

Casting allows some types to take on a different type.  This is
inherently dangerous!  Numeric types can be cast freely between
each other, invoking a potentially lossy conversion.

A reference can only be cast to a pointer with the same target
type.  A pointer can be freely cast between any other pointer,
and can be cast from/to an  *unsigned* integer of the correct
size (64-bits for the AMD64 architecture).

Arrays, structs, and unions can NOT be cast directly, though
pointers to them can be.

```
(cast f32 some-float64)
(cast (^ Blah) some-u64)
```

Casts to the basic types (integers and floats) and custom type
names can use a simpler syntax where the `cast` keyword is
omitted.  Also casting from a reference to a pointer with
the same target type can use the `@` form, which is useful in
data structure initializers.

```
(f32 10203)
(u64 some-ptr)
(BlahPtr some-u64)
(@ some-array-var)
```

When casting values from one integer type to another, we treat
unsigned int as having all higher bits as zero, and signed ints
as having all higher bits as the sign bit (i.e. sign extended).
If the target type is smaller, the higher bits are discarded.

When casting values from floating point to integer, the result
is truncated toward zero (as if the `ftrunc` intrinsic was used).
This intermediate value is a signed integer which is converted
to the target type as per the integer-to-integer casting rules.


Size of a type
--------------

The `(size-of ...)` form produces an untyped constant which is
the size of the given TYPE, in bytes.  For a struct with a open
array at the end, it only counts the part before the array.
It is an error to use this form on `void` or an open array.


Literals
--------

Integers and floating point literals represent *exact* numeric
quantities and have no implicit type.  They are called "untyped".
Character literals just become an integer literal based its
Unicode code point.

These numeric quantities get converted to a specific type at a
place where they are used, such as assigning to a variable or
as a parameter to a function.  It is an error if the type cannot
be deduced or the value cannot be reasonably represented by it.


Built-in Constants
------------------

```
False : the integer 0
True  : the integer 1
Null  : the pointer consisting of all zeros
Void  : the void non-value
```


Constant Definitions
--------------------

A global constant definition gives a name to a literal value,
either an integer, floating point or a string.  Such constants
are (like literals) untyped.  They are defined by a `let` form.
Constants do not "exist" in the output code, they have no
storage and cannot be imported or exported.

```
(let HALF 0.5)
```


Global Variables
----------------

Variables in the global scope are defined via a `var` form.
It requires a type specifier, which describes how the piece of
memory used by the variable is layed out, plus an initial value
to store in that memory.  The value may be an elaborate data
structure.  When the variable appears in code, its type is
actually a *reference* to the specified type, e.g. in the
example below the real type is (ref u8).

```
(var game-over u8 False)
```


Zeroed Variables
----------------

A "zeroed" variable is a global variable whose value exists in
the `BSS` segment of the executable.  These variables do not
take up any space in the executable, instead when the OS loads
the program a block of memory is allocated for them and cleared
to zero.

This is the only way to place variables into the `BSS` segment.
Normal variables are placed in the `DATA` segment.

Note that the type of the variable must have a zero value and
a known size.

```
(zeroed-var k-buffer (array 4000 u8))
```


Function Definitions
--------------------

Each function specifies a parameter list, which has the types
of each parameter and the result type.  The result type can be
omitted if it is void.  Functions also have a code body.

```
(fun print (s (^ String) -> void)
   ; ... body ...
)
```

The parameters and result are limited to basic types, including
references and pointers.  Structs, unions and arrays cannot be
passed directly to a function or returned from one.

Defining a function name is mostly equivalent to having a global
var which is immutable and whose type is a *reference* to a
function type matching the definition.


External Declarations
---------------------

Global vars and functions can be declared as existing in another
code file, using the `extern-var` and `extern-fun` forms.

```
(extern-var foo f64)
(extern-fun bar (a u32 -> u32))
```

An external function is mostly equivalent to an external var
whose type is a reference to a function type.


Local Variables
---------------

The `var` form introduces a mutable local variable into a function.
They are references -- their memory will exist on the stack, hence
they can be any type with a fixed size, and can be assigned new
values via the `set!` form.  The variable name must be followed by
the type of the variable and the initial value.  The sweet syntax
allows an `=` equals symbol in between the type and the value.

The `let` and `let-ref` forms introduce an immutable binding into
the current scope within a function.  The type is not specified and
will be deduced from the expression.  Like parameters, they have no
storage associated with them and are not references, and are limited
to the basic types (including references and pointers).  The `let`
form will automatically dereference a ref type, whereas the `let-ref`
form *requires* a ref type and will not dereference it.

```
(fun foo ()
   (let bar 123)
   (do-something bar 4)

   (var secs (array 20 (^ Section)) { ... })
   (set! [secs 10] (new-section 55))
   (print-section [secs 10])
)
```


Visibility Rules
----------------

Define a "module" as a group of source files which are compiled
together, usually becoming a single object file.

A global variable or function definition is either "private" or
"public".  Private definitions are local to the current module
and are never usable from another module.  Public definitions
can be used by another module via the `extern-xxx` forms.

The keywords `#private` and `#public` can be used anywhere a
global definition can occur, and all subsequent definitions in a
code file will take on the corresponding visibility.  The default
state (before any such keyword) is private.  These keywords have
no effect on declarations marked external.


Assignment
----------

The `set!` form is used to store a value into a variable or
memory location.  The destination must have a reference type, and
it will often be a data access expression in square brackets.

Note that parameters of functions and `let` bindings have no
memory storage -- they are not references and hence cannot be
assigned to.

```
(set! my-var 3)
(set! [my-ptr] Blah)
(set! [my-array 2] 100)
(set! [my-struct .foo] 65)
```


Intrinsics
----------

There are numerous built-in functions which do not generate
actual function calls, but instead become CPU instructions.
For example, `add` takes two integer of the same size and adds
them, producing a result, and it generally becomes the "add"
instruction in the assembly code.

The next few paragraphs describe the current intrinsics...


Integer arithmetic
------------------

```
(ineg I)
(iabs I)

(iadd L R)
(isub L R)
(imul L R)

(idivt N D)  ; truncated division
(idivf N D)  ; floored division (rounded toward -INF)

(iremt N D)  ; uses truncated division, same sign as dividend
(iremf N D)  ; uses floored division,   same sign as divisor
```


Floating point arithmetic
-------------------------

```
(fneg  F)
(fabs  F)
(finv  F)
(fsqrt F)

(fadd L R)
(fsub L R)
(fmul L R)

(fdiv  N D)
(fremt N D)  ; uses truncated division
(fremr N D)  ; uses round-to-nearest division (IEEE 754)

(fpow F EXP)

(fround F)
(ftrunc F)
(ffloor F)
(fceil  F)
```


Bit-wise operations
-------------------

These only apply to integer values.
For the shifts, the count must be an unsigned integer.

```
(bit-not I)

(bit-and L R)
(bit-or  L R)
(bit-xor L R)

(shift-left  I count)
(shift-right I count)
```


Comparisons
-----------

Any basic (non-array non-struct) type can be compared for
equality and inequality.  Only integers, floats and pointers
can be compared by the less/greater intrinsics.

The result of a comparison is a `u8` with the value `0` or `1`.

```
(eq? L R)
(ne? L R)

(lt? L R)
(le? L R)
(gt? L R)
(ge? L R)

(null? P)  ; equivalent to (eq? P Null)
(ref?  P)  ; equivalent to (ne? P Null)

(inf? F)   ; check for floating point infinity
(nan? F)   ; check for floating point NaN

(not I)    ; equivalent to (eq? I False)
```


Miscellaneous
-------------


Flow Control
------------

There is one looping form: `while`, which loops until the condition
expression evaluates to zero (False).

```
(while (is-sun-shining)
   (print Hello-msg)
)
```

There is one conditional form `if`, which can contain optional
`elif` clauses and a optional `else` clause at the end.  The
condition value of `if` and `while` must have an integral type,
where zero is considered false and anything else is true.

```
(if (eq? NumWheels 1)
   (ride-tricycle)
elif (eq? NumWheels 2)
   (ride-bicycle)
else
   (ride-tricycle)
)
```

A "BLOCK" is a group of expressions and local variable definitions.
The `begin` form is used to create an explicit block.  The body of
a function and the code in certain forms like `if` and `while` are
implicit blocks.  Each block is also an expression and produces a
result, generally from the last expression in the block, hence a
block must contain at least one expression.  An exception is the
`begin` form may be empty and this is always void.

The `skip` form allows jumping to a future point in the current
function, essentially skipping the execution of code in-between.
The target is specified with a `junction` form, which can only
be used as the final element of a block.  Junctions have a label
which is just an identifier.  By convention label names are fully
uppercase.  The `skip` form specifies which label to find, and the
corresponding junction MUST exist either at the end of the block
containing the `skip` form, or at the end of some parent block.

The skip form and the corresponding junction may also contain
an expression.  An absent expression is equivalent to void.  The
type of the expression in a skip form must match the one in the
junction.  If control naturally reaches the junction form, its
expression is evaluated and that is the result of the block.
When a skip form is reached, its value is evaluated, control
moves to the end of the block with the matching junction, and
the result of that block is the skip value.

Function bodies have an implicit junction called `RETURN` at the
end (unless the result type is `no-return`).  If the result type
is not void, then the last expression of the body will be the
returned value when no skip is performed.  The `return` form is
equivalent to skip with the `RETURN` label, and it may take a value.

The bodies of `while` loops have an implicit `CONTINUE` junction
at the end, and are enclosed in an implicit block with a `BREAK`
junction at the end.  The `continue` form is equivalent to a skip
with the `CONTINUE` label, and `break` skips to the `BREAK` label.
No value can be given for these.

```
(fun test (cond u8 -> s64)
   (if cond
      (skip FINISH 123)
   )
   (do-something-else)
   (junction FINISH 456)
)
```


Tail Calls
----------

When a function call occurs in a tail position, it will usually
be called as a "tail call", which saves stack space and allows
recursive algorithms to work without overflowing the stack.

Tail calls are inhibited in the following situations:

1. the function being called is external to the module
2. the function is determined to never be recursive
3. a reference to an on-stack variable is passed as a parameter
4. the module is compiled with tail calls disabled

The way a function is determined to be potentially recursive is
firstly to rule out all "leaf" functions, ones that never call
another function.  Then we can rule out all functions that only
call those leaf functions.  And so on...  What remains are the
potentially recursive functions.


Data Structures
---------------

A variable of a complex (non-pointer) type must use a data structure
as the initializer.  The data structure is introduced by a `{` open
curly bracket and terminated by a `}` close curly bracket.

```
(var Primes (array 12 s64)
   { 2 3 5 7  11 13 17 19  23 29 31 37 }
)

(var monster1 Thing
   { .x 12.41 .y 6.73 .type 500 }
)
```

For normal (fixed sized) arrays, the number of elements within
the curly brackets must either equal the size of the array, or
the last element is the `...` triple dots symbol.  The triple
dots indicates all remaining values should take on the zero
value for the element type.

For open (unsized) arrays, there can be any number of elements,
including none at all.  The `...` syntax cannot be used.

For structs, there are two variants.  Either no field names
are used, or there is a field name before each value.  In the
latter case the fields can appear in any order.  For both cases
the final element may be the `...` triple dots symbol to signify
that the remaining (omitted) fields should get their zero value.

For unions, there must be exactly two elements, a field name
(one belonging to the union) followed by the value for it.


Strings
-------

A string literal may appear in certain places within a data
structure or within code.  The type must be an open or fixed
sized array, or a reference or a pointer to one.  The element
type of the array determines how the string is encoded: `u8`
produces UTF-8 (zero terminated), `u16` produces UTF-16 (zero
terminated), and `u32` produces UTF-32 (zero terminated).

When the overall type is a reference or a pointer, the string
will be immutable, e.g. stored in the `TEXT` segment, and a
ref/ptr to its beginning is the final value.  Multiple usages
of the same string (and encoding) become a single instance.

When the overall type is an array, the string is stored in-line
in the data structure or on the stack, and any space left over is
filled with zeros.  It is an error if the string (including the
zero terminator) does not fit.  Such strings are distinct and
can be modified.

A long string can be constructed from several shorter strings
by placing them within `()` parentheses.

```
var str1 (open-array u8) = ("a" "b" "c")
```
