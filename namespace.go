// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
// import "os"
// import "path/filepath"

type Const struct {
	name  string
	value string
	kind  string // "int", "float", "ptr"
}

type Namespace struct {
	consts map[string]*Const
	types  map[string]*Type
	defs   map[string]*GlobalDef
}

// context represents the environment of the code currently being
// compiled.  that code will use this to lookup global identifiers.
var context *Namespace

func NewNamespace() *Namespace {
	ns := new(Namespace)

	ns.consts = make(map[string]*Const)
	ns.types = make(map[string]*Type)
	ns.defs = make(map[string]*GlobalDef)

	return ns
}

func (ns *Namespace) HasConst(name string) bool {
	_, ok := ns.consts[name]
	return ok
}

func (ns *Namespace) GetConst(name string) *Const {
	return ns.consts[name]
}

func (ns *Namespace) AddConst(name string, con *Const) {
	ns.consts[name] = con
}

func (ns *Namespace) RemoveConst(name string) {
	delete(ns.consts, name)
}

func (ns *Namespace) HasType(name string) bool {
	_, ok := ns.types[name]
	return ok
}

func (ns *Namespace) GetType(name string) *Type {
	return ns.types[name]
}

func (ns *Namespace) AddType(name string, ty *Type) {
	ns.types[name] = ty
}

func (ns *Namespace) RemoveType(name string) {
	delete(ns.types, name)
}

func (ns *Namespace) HasDef(name string) bool {
	_, ok := ns.defs[name]
	return ok
}

func (ns *Namespace) GetDef(name string) *GlobalDef {
	return ns.defs[name]
}

func (ns *Namespace) AddDef(name string, def *GlobalDef) {
	ns.defs[name] = def
}

func (ns *Namespace) RemoveDef(name string) {
	delete(ns.defs, name)
}

//----------------------------------------------------------------------

func Const_Parse(t *Token) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 3 {
		PostError("bad const def: missing name or value")
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad const def: extra rubbish at end")
		return FAILED
	}

	t_name := children[1]
	t_val  := children[2]

	if t_name.Kind != TOK_Name {
		PostError("bad const def: name is not an identifier")
		return FAILED
	}
	if ValidateDefName(t_name, "const", 0) != OKAY {
		return FAILED
	}

	con := new(Const)
	con.name = t_name.Str
	con.value = t_val.Str

	switch t_val.Kind {
	case TOK_Int:
		con.kind = "int"

	case TOK_Float:
		con.kind = "float"

	case TOK_String:
		con.kind = "str"

	case TOK_Char:
		runes := []rune(t_val.Str)

		if len(runes) == 0 {
			PostError("malformed character literal")
			return FAILED
		}

		con.value = fmt.Sprintf("%d", runes[0])
		con.kind  = "int"

	default:
		PostError("bad const def: value is not a int/float literal")
		return FAILED
	}

	context.AddConst(con.name, con)

	return OKAY
}

//----------------------------------------------------------------------

type LineGroup struct {
	lines  []string
	indent int
}

func NewLineGroup() *LineGroup {
	lg := new(LineGroup)
	lg.lines = make([]string, 0)
	lg.indent = 0
	return lg
}

func (lg *LineGroup) AddLine(L string) {
	for i := 0; i < lg.indent; i++ {
		L = "\t" + L
	}

	lg.lines = append(lg.lines, L)
}

func (lg *LineGroup) BeginCBlock() {
	lg.AddLine("{")
	lg.indent += 1
}

func (lg *LineGroup) EndCBlock(term string) {
	lg.indent -= 1
	lg.AddLine("}" + term)
}

func (lg *LineGroup) FmtLine(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)

	lg.AddLine(s)
}

func (lg *LineGroup) Output() {
	for _, L := range lg.lines {
		OutLine("%s", L)
	}
}
