
About
-----

This project was an attempt at a fairly low-level language with
a syntax like Scheme or LISP but with semantics like C (but with
less cruft).

It has been superceded by Razm: https://gitlab.com/andwj/razm


Legalese
--------

See the [LICENSE.md](LICENSE.md) file for the terms.

Yew-Leaf comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.

