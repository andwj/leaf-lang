// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
// import "strings"

type BaseType int

const (
	TYP_Void BaseType = iota
	TYP_NoReturn

	TYP_Int
	TYP_Float
	TYP_Pointer
	TYP_Ref

	TYP_Array
	TYP_Struct
	TYP_Union
	TYP_Function

	// TYP_Dummy represents types which are not yet processed
	TYP_Dummy
)

type Type struct {
	base BaseType

	// for TYP_Int and TYP_Float, this is number of bits (8/16/32/64).
	// for TYP_Array, this is number of elements (0 if open).
	// for everything else, this is zero.
	size int

	// the total size of this type (in bytes).
	// -1 means it has not (or could not) be determined yet.
	// not used for open arrays (is zero).
	// for open structs, it is the minimum size.
	// for unions, it is size of the largest field.
	// for pointers, this is always 8 (since arch is 64-bit).
	byte_len int

	// distinguishes u8..u64 from s8..s64
	unsigned bool

	// for TYP_Array and TYP_Struct, indicates it has no fixed size
	open bool

	// this is pointed-to type for pointers and references, the
	// element type for arrays, and return type for functions.
	sub *Type

	// this is "" for plain types, or a name for custom types
	custom string

	// parameters of a function, fields of a tuple, tags of an enum.
	// for interfaces, this is the method signatures, but the "self"
	// parameter in the type is not used (just void).
	param []ParamInfo

	// the yet-to-be-parsed tokens for a type definition
	unparsed *Token

	// the type definition failed to parse
	failed bool
}

type ParamInfo struct {
	// name of function parameter or struct/union field
	name string

	// its type
	ty *Type
}

var (
	void_type *Type
	no_return_type *Type

	s8_type  *Type
	s16_type *Type
	s32_type *Type
	s64_type *Type

	u8_type  *Type
	u16_type *Type
	u32_type *Type
	u64_type *Type

	f32_type *Type
	f64_type *Type
)

//----------------------------------------------------------------------

func SetupTypes() {
	void_type = NewType(TYP_Void, 0)
	no_return_type = NewType(TYP_NoReturn, 0)

	s8_type  = NewType(TYP_Int, 8)
	s16_type = NewType(TYP_Int, 16)
	s32_type = NewType(TYP_Int, 32)
	s64_type = NewType(TYP_Int, 64)

	u8_type  = NewType(TYP_Int, 8)  ; u8_type.unsigned  = true
	u16_type = NewType(TYP_Int, 16) ; u16_type.unsigned = true
	u32_type = NewType(TYP_Int, 32) ; u32_type.unsigned = true
	u64_type = NewType(TYP_Int, 64) ; u64_type.unsigned = true

	f32_type = NewType(TYP_Float, 32)
	f64_type = NewType(TYP_Float, 64)
}

func NewType(base BaseType, size int) *Type {
	ty := new(Type)
	ty.base = base
	ty.size = size
	ty.param = make([]ParamInfo, 0)

	if base == TYP_Int || base == TYP_Float {
		ty.byte_len = size / 8
	} else if base == TYP_Pointer || base == TYP_Ref {
		// assumes 64-bit architecture
		ty.byte_len = 8
	} else if base == TYP_Void || base == TYP_NoReturn {
		ty.byte_len = 0
	} else {
		// actual size computed later
		ty.byte_len = -1
	}

	return ty
}

func (base BaseType) String() string {
	switch base {
	case TYP_Void:
		return "<Void>"
	case TYP_NoReturn:
		return "<NoReturn>"
	case TYP_Int:
		return "<Int>"
	case TYP_Float:
		return "<Float>"
	case TYP_Pointer:
		return "<Ptr>"
	case TYP_Ref:
		return "<Ref>"
	case TYP_Array:
		return "<Array>"
	case TYP_Struct:
		return "<Struct>"
	case TYP_Union:
		return "<Union>"
	case TYP_Function:
		return "<Function>"
	case TYP_Dummy:
		return "!UNPARSED!"
	default:
		return "!!!INVALID!!!"
	}
}

//----------------------------------------------------------------------

func Type_CreateRaw(t *Token) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad type def: missing name")
		return FAILED
	}

//	t_head := children[0]
	t_name := children[1]

	if t_name.Kind != TOK_Name {
		PostError("bad type def: name is not an identifier")
		return FAILED
	}

	if ValidateDefName(t_name, "type", 0) != OKAY {
		return FAILED
	}

	ty := NewType(TYP_Dummy, 0)
	ty.custom = t_name.Str
	ty.unparsed = t

	context.AddType(t_name.Str, ty)

	return OKAY
}

func Type_ParseDef(user *Type) cmError {
	t := user.unparsed

	ErrorSetToken(t)

	children := t.Children

	if len(children) < 3 {
		PostError("bad type def: missing type spec")
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad type def: extra rubbish at end")
		return FAILED
	}

	t_name := children[1]
	t_spec := children[2]

	new_type, err2 := ParseType(t_spec)
	if err2 != OKAY {
		user.failed = true
		return FAILED
	}

	if new_type.base == TYP_Void || new_type.base == TYP_NoReturn {
		PostError("custom type '%s' cannot be void", t_name.Str)
		user.failed = true
		return FAILED
	}
	if new_type.custom != "" {
		PostError("custom type '%s' cannot be another custom type", t_name.Str)
		user.failed = true
		return FAILED
	}

	// replace the dummy place-holder with real one
	user.Replace(new_type)

	return OKAY
}

func Type_CheckDef(ty *Type) cmError {
	if ty.Unconstructable() {
		PostError("type %s is incomplete or invalid", ty.custom)
		ty.failed = true
		return FAILED
	}

	return OKAY
}

//----------------------------------------------------------------------

func ParseType(t *Token) (*Type, cmError) {
	if t.Match("void") { return void_type, OKAY }
	if t.Match("no-return") { return no_return_type, OKAY }

	if t.Match("s8")  { return s8_type,  OKAY }
	if t.Match("s16") { return s16_type, OKAY }
	if t.Match("s32") { return s32_type, OKAY }
	if t.Match("s64") { return s64_type, OKAY }

	if t.Match("u8")  { return u8_type,  OKAY }
	if t.Match("u16") { return u16_type, OKAY }
	if t.Match("u32") { return u32_type, OKAY }
	if t.Match("u64") { return u64_type, OKAY }

	if t.Match("f32") { return f32_type, OKAY }
	if t.Match("f64") { return f64_type, OKAY }

	// check for user defined types
	if t.Kind == TOK_Name {
		user := context.GetType(t.Str)
		if user != nil {
			return user, OKAY
		}

		PostError("unknown type '%s'", t.Str)
		return nil, FAILED
	}

	if t.Kind != TOK_Expr || len(t.Children) < 2 {
		PostError("malformed type, got: %s", t.String())
		return nil, FAILED
	}

	return ParseCompoundType(t.Children)
}

func ParseCompoundType(children []*Token) (*Type, cmError) {
	head := children[0]

	if head.Match("^") {
		var sub *Type
		var err2 cmError

		if len(children) < 2 {
			PostError("malformed pointer type")
			return nil, FAILED
		} else if len(children) == 2 {
			sub, err2 = ParseType(children[1])
		} else {
			// support abbreviated types like: (^ ^ u8)
			sub, err2 = ParseCompoundType(children[1:])
		}

		if err2 != OKAY {
			return nil, FAILED
		}

		if sub.base == TYP_Void || sub.base == TYP_NoReturn {
			PostError("pointer target cannot be void")
			return nil, FAILED
		}

		res := sub.MakePointerType()
		return res, OKAY
	}

	if head.Match("ref") {
		var sub *Type
		var err2 cmError

		if len(children) < 2 {
			PostError("malformed reference type")
			return nil, FAILED
		} else if len(children) == 2 {
			sub, err2 = ParseType(children[1])
		} else {
			// support abbreviated types
			sub, err2 = ParseCompoundType(children[1:])
		}

		if err2 != OKAY {
			return nil, FAILED
		}

		if sub.base == TYP_Void || sub.base == TYP_NoReturn {
			PostError("reference target cannot be void")
			return nil, FAILED
		}

		res := sub.MakeRefType()
		return res, OKAY
	}

	if head.Match("array") {
		if len(children) != 3 {
			PostError("malformed array type")
			return nil, FAILED
		}

		size := 0

		{
			t_size := children[1]

			size_str := ""

			switch t_size.Kind {
			case TOK_Int:
				size_str = t_size.Str

			case TOK_Name:
				// check for a user constant
				con := context.GetConst(t_size.Str)
				if con == nil {
					PostError("expected array size, got '%s'", t_size.Str)
					return nil, FAILED
				}
				if con.kind != "int" {
					PostError("bad array size, '%s' is not an integer", t_size.Str)
					return nil, FAILED
				}
				size_str = con.value

			default:
				PostError("expected array size, got %s", t_size.String())
				return nil, FAILED
			}

			var err error
			size, err = strconv.Atoi(size_str)
			if err != nil {
				PostError("bad array size: %s", size_str)
				return nil, FAILED
			}
			if size < 1 || size > 0x7FFFFFFF {
				PostError("bad array size: %s", size_str)
				return nil, FAILED
			}

			children = children[1:]
		}

		sub, err2 := ParseType(children[1])
		if err2 != OKAY {
			return nil, FAILED
		}

		if sub.base == TYP_Void ||
			sub.base == TYP_NoReturn ||
			sub.base == TYP_Function ||
			sub.open {

			PostError("bad type for array elements: %s", sub.String())
			return nil, FAILED
		}

		res := NewType(TYP_Array, size)
		res.sub = sub

		if !res.TryCalcByteLength() {
			// FIXME DO SOMETHING
		}

		return res, OKAY
	}

	if head.Match("open-array") {
		if len(children) != 2 {
			PostError("malformed array type")
			return nil, FAILED
		}

		sub, err2 := ParseType(children[1])
		if err2 != OKAY {
			return nil, FAILED
		}

		if sub.base == TYP_Void ||
			sub.base == TYP_NoReturn ||
			sub.base == TYP_Function ||
			sub.open {

			PostError("bad type for array elements: %s", sub.String())
			return nil, FAILED
		}

		res := NewType(TYP_Array, 0)
		res.open = true
		res.sub = sub

		if !res.TryCalcByteLength() {
			// FIXME DO SOMETHING
		}

		return res, OKAY
	}

	if head.Match("struct") || head.Match("open-struct") {
		children = children[1:]
		count := len(children)

		if count < 1 || count%2 != 0 {
			PostError("malformed struct type")
			return nil, FAILED
		}

		count /= 2

		res := NewType(TYP_Struct, 0)
		res.open = head.Match("open-struct")

		// get types for each field
		for i := 0; i < count; i++ {
			is_last := (i == count-2)

			t_field := children[i*2]
			t_type := children[i*2+1]

			if !t_field.IsNamedField() {
				PostError("bad field name in struct: %s", t_field.Str)
				return nil, FAILED
			}

			field_name := t_field.Str

			sub, err2 := ParseType(t_type)
			if err2 != OKAY {
				return nil, FAILED
			}

			if sub.base == TYP_Void ||
				sub.base == TYP_NoReturn ||
				sub.base == TYP_Function ||
				(sub.open && !res.open) {

				PostError("bad type for struct field: %s", sub.String())
				return nil, FAILED
			}

			if sub.open && !is_last {
				PostError("open type in struct must be at end")
				return nil, FAILED
			}

			res.AddParam(field_name, sub)
		}

		if !res.TryCalcByteLength() {
			// FIXME DO SOMETHING
		}

		return res, OKAY
	}

	if head.Match("union") {
		children = children[1:]

		count := len(children)

		if count == 0 || count%2 != 0 {
			PostError("malformed union type")
			return nil, FAILED
		}

		count /= 2

		res := NewType(TYP_Union, 0)

		// get each tag keyword and associated data type
		for i := 0; i < count; i++ {
			t_field := children[i*2]
			t_datum := children[i*2+1]

			if !t_field.IsNamedField() {
				PostError("bad field name in union: %s", t_field.Str)
				return nil, FAILED
			}

			sub, err2 := ParseType(t_datum)
			if err2 != OKAY {
				return nil, FAILED
			}

			if sub.base == TYP_Void ||
				sub.base == TYP_NoReturn ||
				sub.base == TYP_Function ||
				sub.open {

				PostError("bad type for union field: %s", sub.String())
				return nil, FAILED
			}

			res.AddParam(t_field.Str, sub)
		}

		if !res.TryCalcByteLength() {
			// FIXME DO SOMETHING
		}

		return res, OKAY
	}

	if head.Match("function") {
		num := len(children)

		if num < 2 {
			PostError("malformed function type")
			return nil, FAILED
		}

		// the return type is last
		sub, err2 := ParseType(children[num-1])
		if err2 != OKAY {
			return nil, FAILED
		}
		if !sub.IsBasic() {
			PostError("result type must be basic, got: %s",
				sub.String())
			return nil, FAILED
		}

		res := NewType(TYP_Function, 0)
		res.sub = sub

		// get types for each parameter
		for i := 1; i < num-1; i++ {
			par_type, err2 := ParseType(children[i])
			if err2 != OKAY {
				return nil, FAILED
			}

			if par_type.base == TYP_Void || par_type.base == TYP_NoReturn {
				PostError("parameters cannot be void")
				return nil, FAILED
			}
			if !par_type.IsBasic() {
				PostError("parameter type must be basic, got: %s",
					par_type.String())
				return nil, FAILED
			}

			res.AddParam("", par_type)
		}

		return res, OKAY
	}

	if head.Kind == TOK_Name {
		PostError("unknown type: %s", head.Str)
		return nil, FAILED
	} else {
		PostError("malformed type, got: %s", head.String())
		return nil, FAILED
	}
}

func IsTypeName(name string) bool {
	switch name {
	case "void", "no-return":
		return true
	case "s8", "s16", "s32", "s64":
		return true
	case "u8", "u16", "u32", "u64":
		return true
	case "f32", "f64":
		return true
	}

	return context.HasType(name)
}

func (ty *Type) IsBasic() bool {
	switch ty.base {
	case TYP_Void, TYP_NoReturn:
		return true
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Ref:
		return true
	default:
		return false
	}
}

func (ty *Type) TryCalcByteLength() bool {
	// FIXME
	return false
}

func (ty *Type) String() string {
	if ty == nil {
		return "(nil)"
		// panic("nil given to Type.String()")
	}

	if ty.custom != "" {
		return ty.custom
	}

	switch ty.base {
	case TYP_Void:
		return "void"

	case TYP_NoReturn:
		return "no-return"

	case TYP_Int:
		s := "s"
		if ty.unsigned {
			s = "u"
		}
		return fmt.Sprintf("%s%d", s, ty.size)

	case TYP_Float:
		if ty.size == 32 {
			return "f32"
		} else {
			return "f64"
		}

	case TYP_Pointer:
		return "(^ " + ty.sub.String() + ")"

	case TYP_Ref:
		return "(ref " + ty.sub.String() + ")"

	case TYP_Array:
		size := ""
		if !ty.open {
			size = strconv.Itoa(ty.size) + " "
		}
		return "(array " + size + ty.sub.String() + ")"

	case TYP_Struct:
		s := "(struct"
		for _, par := range ty.param {
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Union:
		s := "(union"
		for _, par := range ty.param {
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Function:
		s := "(function "

		for _, par := range ty.param {
			s = s + par.ty.String() + " "
		}

		return s + ty.sub.String() + ")"

	default:
		return "!!BAD-TYPE!!"
	}
}

func (ty *Type) Replace(other *Type) {
	// NOTE: we do not overwrite the custom name

	ty.base = other.base
	ty.size = other.size

	ty.byte_len = other.byte_len
	ty.unsigned = other.unsigned
	ty.open = other.open

	ty.sub = other.sub
	ty.param = other.param
}

func (ty *Type) AddParam(name string, par_ty *Type) {
	ty.param = append(ty.param, ParamInfo{name: name, ty: par_ty})
}

func (ty *Type) FindParam(name string) int {
	for i := range ty.param {
		if ty.param[i].name == name {
			return i
		}
	}

	// not found
	return -1
}

func (ty *Type) MakePointerType() *Type {
	new_ty := NewType(TYP_Pointer, 0)
	new_ty.sub = ty
	new_ty.byte_len = 8

	return new_ty
}

func (ty *Type) MakeRefType() *Type {
	new_ty := NewType(TYP_Ref, 0)
	new_ty.sub = ty
	new_ty.byte_len = 8

	return new_ty
}

//----------------------------------------------------------------------

func (src *Type) AssignableTo(dest *Type) bool {
	if src == nil || dest == nil {
		panic("AssignableTo with nil type")
	}

	if src == dest {
		return true
	}

	if src.base != dest.base {
		return false
	}

	// if both types are custom types, they must be the exact same one
	if src.custom != "" && dest.custom != "" {
		return src.custom == dest.custom
	}

	// void is never really assigned, but we need to handle function results
	if src.base == TYP_Void || src.base == TYP_NoReturn {
		return true
	}

	// integers and floats require same size and signed-ness
	if src.base == TYP_Int || src.base == TYP_Float {
		if src.size != dest.size || src.unsigned != dest.unsigned {
			return false
		}
		return true
	}

	// pointers must point at compatible types
	if src.base == TYP_Pointer || src.base == TYP_Ref {
		return src.sub.AssignableTo(dest.sub)
	}

	// arrays must be same size, have compatible elements
	if src.base == TYP_Array {
		if src.open != dest.open {
			return false
		}
		if !src.open && src.size != dest.size {
			return false
		}
		return src.sub.AssignableTo(dest.sub)
	}

	// for structs and unions, check all fields are assignable.
	// also require same field names
	if src.base == TYP_Struct || src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			return false
		}
		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				return false
			}

			if !src.param[i].ty.AssignableTo(dest.param[i].ty) {
				return false
			}
		}
		return true
	}

	// for functions, check parameters and result type.
	if src.base == TYP_Function {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.AssignableTo(dest.param[i].ty) {
				return false
			}
		}

		// check return type
		return src.sub.AssignableTo(dest.sub)
	}

	return false
}

func (src *Type) CastableTo(dest *Type) bool {
	if src == nil || dest == nil {
		panic("CastableTo with nil type")
	}

	// allow casting anything to void
	if dest.base == TYP_Void {
		return true
	}

	// allow casting between void and `no-return'
	if dest.base == TYP_NoReturn && src.base == TYP_Void {
		return true
	}

	// can freely cast between numeric types (invoking a conversion)
	s_numeric := (src.base  == TYP_Int || src.base  == TYP_Float)
	d_numeric := (dest.base == TYP_Int || dest.base == TYP_Float)

	if s_numeric && d_numeric {
		return true
	}

	// can cast between a reference and a pointer if their target
	// types are the same
	s_pointery := (src.base  == TYP_Pointer || src.base  == TYP_Ref)
	d_pointery := (dest.base == TYP_Pointer || dest.base == TYP_Ref)

	if s_pointery && d_pointery {
		if src.sub.StructurallyEqual(dest.sub) {
			return true
		}
	}

	// allow cast between a pointer and unsigned int of same size
	// [ Note: this code assumes a 64-bit arch like AMD64 ]
	if src.base == TYP_Pointer &&
		dest.base == TYP_Int && dest.size == 64 && dest.unsigned {
		return true
	}
	if dest.base == TYP_Pointer &&
		src.base == TYP_Int && src.size == 64 && src.unsigned {
		return true
	}

	return src.StructurallyEqual(dest)
}

func (src *Type) StructurallyEqual(dest *Type) bool {
	if src == nil || dest == nil {
		panic("StructurallyEqual with nil type")
	}

	if src.base != dest.base {
		return false
	}

	if src.base == TYP_Void || src.base == TYP_NoReturn {
		return true
	}

	// for integers and floats, require same size.
	// we ignore signed-ness of integers here.
	if src.base == TYP_Int || src.base == TYP_Float {
		return src.size == dest.size
	}

	// pointers are freely castable  [ very dangerous though ]
	if src.base == TYP_Pointer {
		return true
	}

	// reference targets must have same structure
	if src.base == TYP_Pointer {
		return src.sub.StructurallyEqual(dest.sub)
	}

	// arrays need same length (or both be open)
	if src.base == TYP_Array {
		if src.open != dest.open {
			return false
		}
		if !src.open && src.size != dest.size {
			return false
		}
		return src.sub.StructurallyEqual(dest.sub)
	}

	// for structs and unions, check all fields are similar enough
	if src.base == TYP_Struct || src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			return false
		}
		for i := range src.param {
			if !src.param[i].ty.StructurallyEqual(dest.param[i].ty) {
				return false
			}
		}
		return true
	}

	// no need to worry about functions (we always use pointers to them).
	// and struct fields or array elements are never `void`.

	return false
}

//----------------------------------------------------------------------

// Unconstructable detects whether this type cannot ever be
// constructed (due to an unbreakable cyclic reference).
func (ty *Type) Unconstructable() bool {
	seen := make(map[*Type]bool)

	return !ty.canConstruct(seen)
}

func (T *Type) canConstruct(seen map[*Type]bool) bool {
	// only structs and unions can form "unusable" cyclic refs.
	// for example, new arrays and maps can be empty.
	if !(T.base == TYP_Struct || T.base == TYP_Union) {
		return true
	}

	// if we reach an already visited type, it means the current
	// "path" from original type to this one has a cycle in it
	// and hence is unusable.
	if seen[T] {
		return false
	}

	// recreate the seen map with new type in it
	new_seen := make(map[*Type]bool)
	for S, _ := range seen {
		new_seen[S] = true
	}
	new_seen[T] = true

	// for unions we only need ONE of its tags to be constructable.
	if T.base == TYP_Union {
		for _, par := range T.param {
			if par.ty.canConstruct(new_seen) {
				return true
			}
		}
		return false
	}

	// for structs we require ALL fields to be constructable.
	if T.base == TYP_Struct {
		for _, par := range T.param {
			if !par.ty.canConstruct(new_seen) {
				return false
			}
		}
	}

	return true
}
