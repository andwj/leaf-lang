;;
;; Linux AMD64 ABI
;;

;
; syscalls
;
%define sys_read		0
%define sys_write		1
%define sys_open		2
%define sys_close		3
%define sys_stat		4
%define sys_fstat		5
%define sys_lstat		6
%define sys_poll		7
%define sys_lseek		8
%define sys_mmap		9
%define sys_mprotect		10
%define sys_munmap		11
%define sys_brk			12
%define sys_rt_sigaction	13
%define sys_rt_sigprocmask	14
%define sys_rt_sigreturn	15
%define sys_ioctl		16
%define sys_pread64		17
%define sys_pwrite64		18
%define sys_readv		19
%define sys_writev		20
%define sys_access		21
%define sys_pipe		22
%define sys_select		23
%define sys_sched_yield		24
%define sys_mremap		25
%define sys_msync		26
%define sys_mincore		27
%define sys_madvise		28
%define sys_shmget		29
%define sys_shmat		30
%define sys_shmctl		31
%define sys_dup			32
%define sys_dup2		33
%define sys_pause		34
%define sys_nanosleep		35
%define sys_getitimer		36
%define sys_alarm		37
%define sys_setitimer		38
%define sys_getpid		39
%define sys_sendfile		40
%define sys_socket		41
%define sys_connect		42
%define sys_accept		43
%define sys_sendto		44
%define sys_recvfrom		45
%define sys_sendmsg		46
%define sys_recvmsg		47
%define sys_shutdown		48
%define sys_bind		49
%define sys_listen		50
%define sys_getsockname		51
%define sys_getpeername		52
%define sys_socketpair		53
%define sys_setsockopt		54
%define sys_getsockopt		55
%define sys_clone		56
%define sys_fork		57
%define sys_vfork		58
%define sys_execve		59
%define sys_exit		60
%define sys_wait4		61
%define sys_kill		62
%define sys_uname		63
%define sys_semget		64
%define sys_semop		65
%define sys_semctl		66
%define sys_shmdt		67
%define sys_msgget		68
%define sys_msgsnd		69
%define sys_msgrcv		70
%define sys_msgctl		71
%define sys_fcntl		72
%define sys_flock		73
%define sys_fsync		74
%define sys_fdatasync		75
%define sys_truncate		76
%define sys_ftruncate		77
%define sys_getdents		78
%define sys_getcwd		79
%define sys_chdir		80
%define sys_fchdir		81
%define sys_rename		82
%define sys_mkdir		83
%define sys_rmdir		84
%define sys_creat		85
%define sys_link		86
%define sys_unlink		87
%define sys_symlink		88
%define sys_readlink		89
%define sys_chmod		90
%define sys_fchmod		91
%define sys_chown		92
%define sys_fchown		93
%define sys_lchown		94
%define sys_umask		95
%define sys_gettimeofday	96
%define sys_getrlimit		97
%define sys_getrusage		98
%define sys_sysinfo		99
%define sys_times		100
%define sys_ptrace		101
%define sys_getuid		102
%define sys_syslog		103
%define sys_getgid		104
%define sys_setuid		105
%define sys_setgid		106
%define sys_geteuid		107
%define sys_getegid		108
%define sys_setpgid		109
%define sys_getppid		110
%define sys_getpgrp		111
%define sys_setsid		112
%define sys_setreuid		113
%define sys_setregid		114
%define sys_getgroups		115
%define sys_setgroups		116
%define sys_setresuid		117
%define sys_getresuid		118
%define sys_setresgid		119
%define sys_getresgid		120
%define sys_getpgid		121
%define sys_setfsuid		122
%define sys_setfsgid		123
%define sys_getsid		124
%define sys_capget		125
%define sys_capset		126
%define sys_rt_sigpending	127
%define sys_rt_sigtimedwait	128
%define sys_rt_sigqueueinfo	129
%define sys_rt_sigsuspend	130
%define sys_sigaltstack		131
%define sys_utime		132
%define sys_mknod		133
%define sys_uselib		134
%define sys_personality		135
%define sys_ustat		136
%define sys_statfs		137
%define sys_fstatfs		138
%define sys_sysfs		139
%define sys_getpriority		140
%define sys_setpriority		141
%define sys_sched_setparam	142
%define sys_sched_getparam	143
%define sys_sched_setscheduler	144
%define sys_sched_getscheduler	145
%define sys_sched_get_priority_max	146
%define sys_sched_get_priority_min	147
%define sys_sched_rr_get_interval	148
%define sys_mlock		149
%define sys_munlock		150
%define sys_mlockall		151
%define sys_munlockall		152
%define sys_vhangup		153
%define sys_modify_ldt		154
%define sys_pivot_root		155
%define sys__sysctl		156
%define sys_prctl		157
%define sys_arch_prctl		158
%define sys_adjtimex		159
%define sys_setrlimit		160
%define sys_chroot		161
%define sys_sync		162
%define sys_acct		163
%define sys_settimeofday	164
%define sys_mount		165
%define sys_umount2		166
%define sys_swapon		167
%define sys_swapoff		168
%define sys_reboot		169
%define sys_sethostname		170
%define sys_setdomainname	171
%define sys_iopl		172
%define sys_ioperm		173
%define sys_create_module	174
%define sys_init_module		175
%define sys_delete_module	176
%define sys_get_kernel_syms	177
%define sys_query_module	178
%define sys_quotactl		179
%define sys_nfsservctl		180
%define sys_getpmsg		181
%define sys_putpmsg		182
%define sys_afs_syscall		183
%define sys_tuxcall		184
%define sys_security		185
%define sys_gettid		186
%define sys_readahead		187
%define sys_setxattr		188
%define sys_lsetxattr		189
%define sys_fsetxattr		190
%define sys_getxattr		191
%define sys_lgetxattr		192
%define sys_fgetxattr		193
%define sys_listxattr		194
%define sys_llistxattr		195
%define sys_flistxattr		196
%define sys_removexattr		197
%define sys_lremovexattr	198
%define sys_fremovexattr	199
%define sys_tkill		200
%define sys_time		201
%define sys_futex		202
%define sys_sched_setaffinity	203
%define sys_sched_getaffinity	204
%define sys_set_thread_area	205
%define sys_io_setup		206
%define sys_io_destroy		207
%define sys_io_getevents	208
%define sys_io_submit		209
%define sys_io_cancel		210
%define sys_get_thread_area	211
%define sys_lookup_dcookie	212
%define sys_epoll_create	213
%define sys_epoll_ctl_old	214
%define sys_epoll_wait_old	215
%define sys_remap_file_pages	216
%define sys_getdents64		217
%define sys_set_tid_address	218
%define sys_restart_syscall	219
%define sys_semtimedop		220
%define sys_fadvise64		221
%define sys_timer_create	222
%define sys_timer_settime	223
%define sys_timer_gettime	224
%define sys_timer_getoverrun	225
%define sys_timer_delete	226
%define sys_clock_settime	227
%define sys_clock_gettime	228
%define sys_clock_getres	229
%define sys_clock_nanosleep	230
%define sys_exit_group		231
%define sys_epoll_wait		232
%define sys_epoll_ctl		233
%define sys_tgkill		234
%define sys_utimes		235
%define sys_vserver		236
%define sys_mbind		237
%define sys_set_mempolicy	238
%define sys_get_mempolicy	239
%define sys_mq_open		240
%define sys_mq_unlink		241
%define sys_mq_timedsend	242
%define sys_mq_timedreceive	243
%define sys_mq_notify		244
%define sys_mq_getsetattr	245
%define sys_kexec_load		246
%define sys_waitid		247
%define sys_add_key		248
%define sys_request_key		249
%define sys_keyctl		250
%define sys_ioprio_set		251
%define sys_ioprio_get		252
%define sys_inotify_init	253
%define sys_inotify_add_watch	254
%define sys_inotify_rm_watch	255
%define sys_migrate_pages	256
%define sys_openat		257
%define sys_mkdirat		258
%define sys_mknodat		259
%define sys_fchownat		260
%define sys_futimesat		261
%define sys_newfstatat		262
%define sys_unlinkat		263
%define sys_renameat		264
%define sys_linkat		265
%define sys_symlinkat		266
%define sys_readlinkat		267
%define sys_fchmodat		268
%define sys_faccessat		269
%define sys_pselect6		270
%define sys_ppoll		271
%define sys_unshare		272
%define sys_set_robust_list	273
%define sys_get_robust_list	274
%define sys_splice		275
%define sys_tee			276
%define sys_sync_file_range	277
%define sys_vmsplice		278
%define sys_move_pages		279
%define sys_utimensat		280
%define sys_epoll_pwait		281
%define sys_signalfd		282
%define sys_timerfd_create	283
%define sys_eventfd		284
%define sys_fallocate		285
%define sys_timerfd_settime	286
%define sys_timerfd_gettime	287
%define sys_accept4		288
%define sys_signalfd4		289
%define sys_eventfd2		290
%define sys_epoll_create1	291
%define sys_dup3		292
%define sys_pipe2		293
%define sys_inotify_init1	294
%define sys_preadv		295
%define sys_pwritev		296
%define sys_rt_tgsigqueueinfo	297
%define sys_perf_event_open	298
%define sys_recvmmsg		299
%define sys_fanotify_init	300
%define sys_fanotify_mark	301
%define sys_prlimit64		302
%define sys_name_to_handle_at	303
%define sys_open_by_handle_at	304
%define sys_clock_adjtime	305
%define sys_syncfs		306
%define sys_sendmmsg		307
%define sys_setns		308
%define sys_getcpu		309
%define sys_process_vm_readv	310
%define sys_process_vm_writev	311
%define sys_kcmp		312
%define sys_finit_module	313
%define sys_sched_setattr	314
%define sys_sched_getattr	315
%define sys_renameat2		316
%define sys_seccomp		317
%define sys_getrandom		318
%define sys_memfd_create	319
%define sys_kexec_file_load	320
%define sys_bpf			321
%define sys_execveat		322
%define sys_userfaultfd		323
%define sys_membarrier		324
%define sys_mlock2		325
%define sys_copy_file_range	326
%define sys_preadv2		327
%define sys_pwritev2		328
%define sys_pkey_mprotect	329
%define sys_pkey_alloc		330
%define sys_pkey_free		331

;
; standard file descriptors
;
%define STDIN		0
%define STDOUT		1
%define STDERR		2

;
; lseek(2) whence values
;
%define SEEK_SET	0
%define SEEK_CUR	1
%define SEEK_END	2

;
; open(2) flags
;
%define O_RDONLY	0	;
%define O_WRONLY	1	; access modes
%define O_RDWR		2	;

%define O_CREAT		0100q
%define O_EXCL		0200q
%define O_NOCTTY	0400q
%define O_TRUNC		1000q
%define O_APPEND	2000q
%define O_NONBLOCK	4000q
%define O_NONDELAY	4000q

%define O_DSYNC		10000q
%define O_ASYNC		20000q
%define O_DIRECT	40000q
%define O_LARGEFILE	100000q
%define O_DIRECTORY	200000q
%define O_NOFOLLOW	400000q
%define O_NOATIME	1000000q

;
; mode bits for stat(2), creat(2) etc..
;
%define S_IRWXU		700q	;
%define S_IRUSR		400q	; user R/W/X
%define S_IWUSR		200q	;
%define S_IXUSR		100q	;

%define S_IRWXG		70q	;
%define S_IRGRP		40q	; group R/W/X
%define S_IWGRP		20q	;
%define S_IXGRP		10q	;

%define S_IRWXO		7q	;
%define S_IROTH		4q	; other R/W/X
%define S_IWOTH		2q	;
%define S_IXOTH		1q	;

%define S_ISUID		4000q	; set-user-ID
%define S_ISGID		2000q	; set-group-ID
%define S_ISVTX		1000q	; sticky bit

%define S_IFIFO		10000q	; FIFO
%define S_IFCHR		20000q	; character device
%define S_IFDIR		40000q	; directory
%define S_IFBLK		60000q	; block device
%define S_IFREG		100000q	; regular file
%define S_IFLNK		120000q	; symbol link
%define S_IFSOCK	140000q	; socket
%define S_IFMT		170000q	; (bitmask for above types)

;
; mmap(2) flags
;
%define PROT_READ	1
%define PROT_WRITE	2
%define PROT_EXEC	4
%define PROT_NONE	0

%define MAP_SHARED	0x01	; Share changes
%define MAP_PRIVATE	0x02	; Changes are private
%define MAP_FIXED	0x10	; Interpret addr exactly
%define MAP_ANONYMOUS	0x20	; don't use a file
%define MAP_32BIT	0x40	; only give out 32bit addresses

%define MAP_GROWSDOWN	0x00100  ; stack-like segment
%define MAP_DENYWRITE	0x00800  ; ETXTBSY
%define MAP_EXECUTABLE	0x01000  ; mark it as an executable
%define MAP_LOCKED	0x02000  ; pages are locked
%define MAP_NORESERVE	0x04000  ; don't check for reservations
%define MAP_POPULATE	0x08000  ; populate (prefault) pagetables
%define MAP_NONBLOCK	0x10000  ; do not block on IO
%define MAP_STACK	0x20000  ; allocation is for a stack

;
; signals
;
%define SIGHUP		1
%define SIGINT		2
%define SIGQUIT		3
%define SIGILL		4
%define SIGTRAP		5
%define SIGABRT		6
%define SIGIOT		6
%define SIGBUS		7
%define SIGFPE		8
%define SIGKILL		9
%define SIGUSR1		10
%define SIGSEGV		11
%define SIGUSR2		12
%define SIGPIPE		13
%define SIGALRM		14
%define SIGTERM		15
%define SIGSTKFLT	16
%define SIGCHLD		17
%define SIGCONT		18
%define SIGSTOP		19
%define SIGTSTP		20
%define SIGTTIN		21
%define SIGTTOU		22
%define SIGURG		23
%define SIGXCPU		24
%define SIGXFSZ		25
%define SIGVTALRM	26
%define SIGPROF		27
%define SIGWINCH	28
%define SIGIO		29
%define SIGPOLL		29	; same as SIGIO
%define SIGPWR		30
%define SIGSYS		31
%define SIGUNUSED	31
%define SIGRTMIN	32

;
; errno
;
%define EPERM		 1	; Operation not permitted
%define ENOENT		 2	; No such file or directory
%define ESRCH		 3	; No such process
%define EINTR		 4	; Interrupted system call
%define EIO		 5	; I/O error
%define ENXIO		 6	; No such device or address
%define E2BIG		 7	; Argument list too long
%define ENOEXEC		 8	; Exec format error
%define EBADF		 9	; Bad file number
%define ECHILD		10	; No child processes
%define EAGAIN		11	; Try again
%define EWOULDBLOCK	11	; Operation would block
%define ENOMEM		12	; Out of memory
%define EACCES		13	; Permission denied
%define EFAULT		14	; Bad address
%define ENOTBLK		15	; Block device required
%define EBUSY		16	; Device or resource busy
%define EEXIST		17	; File exists
%define EXDEV		18	; Cross-device link
%define ENODEV		19	; No such device
%define ENOTDIR		20	; Not a directory
%define EISDIR		21	; Is a directory
%define EINVAL		22	; Invalid argument
%define ENFILE		23	; File table overflow
%define EMFILE		24	; Too many open files
%define ENOTTY		25	; Not a typewriter
%define ETXTBSY		26	; Text file busy
%define EFBIG		27	; File too large
%define ENOSPC		28	; No space left on device
%define ESPIPE		29	; Illegal seek
%define EROFS		30	; Read-only file system
%define EMLINK		31	; Too many links
%define EPIPE		32	; Broken pipe
%define EDOM		33	; Math argument out of domain of func
%define ERANGE		34	; Math result not representable
%define EDEADLK		35	; Resource deadlock would occur
%define EDEADLOCK	35
%define ENAMETOOLONG	36	; File name too long
%define ENOLCK		37	; No record locks available
%define ENOSYS		38	; Invalid system call number

%define ENOTEMPTY	39	; Directory not empty
%define ELOOP		40	; Too many symbolic links encountered
%define ENOMSG		42	; No message of desired type
%define EIDRM		43	; Identifier removed
%define ECHRNG		44	; Channel number out of range
%define EL2NSYNC	45	; Level 2 not synchronized
%define EL3HLT		46	; Level 3 halted
%define EL3RST		47	; Level 3 reset
%define ELNRNG		48	; Link number out of range
%define EUNATCH		49	; Protocol driver not attached
%define ENOCSI		50	; No CSI structure available
%define EL2HLT		51	; Level 2 halted
%define EBADE		52	; Invalid exchange
%define EBADR		53	; Invalid request descriptor
%define EXFULL		54	; Exchange full
%define ENOANO		55	; No anode
%define EBADRQC		56	; Invalid request code
%define EBADSLT		57	; Invalid slot

%define EBFONT		59	; Bad font file format
%define ENOSTR		60	; Device not a stream
%define ENODATA		61	; No data available
%define ETIME		62	; Timer expired
%define ENOSR		63	; Out of streams resources
%define ENONET		64	; Machine is not on the network
%define ENOPKG		65	; Package not installed
%define EREMOTE		66	; Object is remote
%define ENOLINK		67	; Link has been severed
%define EADV		68	; Advertise error
%define ESRMNT		69	; Srmount error
%define ECOMM		70	; Communication error on send
%define EPROTO		71	; Protocol error
%define EMULTIHOP	72	; Multihop attempted
%define EDOTDOT		73	; RFS specific error
%define EBADMSG		74	; Not a data message
%define EOVERFLOW	75	; Value too large for defined data type
%define ENOTUNIQ	76	; Name not unique on network
%define EBADFD		77	; File descriptor in bad state
%define EREMCHG		78	; Remote address changed
%define ELIBACC		79	; Can not access a needed shared library
%define ELIBBAD		80	; Accessing a corrupted shared library
%define ELIBSCN		81	; .lib section in a.out corrupted
%define ELIBMAX		82	; Attempting to link in too many shared libraries
%define ELIBEXEC	83	; Cannot exec a shared library directly
%define EILSEQ		84	; Illegal byte sequence
%define ERESTART	85	; Interrupted system call should be restarted
%define ESTRPIPE	86	; Streams pipe error
%define EUSERS		87	; Too many users
%define ENOTSOCK	88	; Socket operation on non-socket
%define EDESTADDRREQ	89	; Destination address required
%define EMSGSIZE	90	; Message too long
%define EPROTOTYPE	91	; Protocol wrong type for socket
%define ENOPROTOOPT	92	; Protocol not available
%define EPROTONOSUPPORT	93	; Protocol not supported
%define ESOCKTNOSUPPORT	94	; Socket type not supported
%define EOPNOTSUPP	95	; Operation not supported on transport endpoint
%define EPFNOSUPPORT	96	; Protocol family not supported
%define EAFNOSUPPORT	97	; Address family not supported by protocol
%define EADDRINUSE	98	; Address already in use
%define EADDRNOTAVAIL	99	; Cannot assign requested address
%define ENETDOWN	100	; Network is down
%define ENETUNREACH	101	; Network is unreachable
%define ENETRESET	102	; Network dropped connection because of reset
%define ECONNABORTED	103	; Software caused connection abort
%define ECONNRESET	104	; Connection reset by peer
%define ENOBUFS		105	; No buffer space available
%define EISCONN		106	; Transport endpoint is already connected
%define ENOTCONN	107	; Transport endpoint is not connected
%define ESHUTDOWN	108	; Cannot send after transport endpoint shutdown
%define ETOOMANYREFS	109	; Too many references: cannot splice
%define ETIMEDOUT	110	; Connection timed out
%define ECONNREFUSED	111	; Connection refused
%define EHOSTDOWN	112	; Host is down
%define EHOSTUNREACH	113	; No route to host
%define EALREADY	114	; Operation already in progress
%define EINPROGRESS	115	; Operation now in progress
%define ESTALE		116	; Stale file handle
%define EUCLEAN		117	; Structure needs cleaning
%define ENOTNAM		118	; Not a XENIX named type file
%define ENAVAIL		119	; No XENIX semaphores available
%define EISNAM		120	; Is a named type file
%define EREMOTEIO	121	; Remote I/O error
%define EDQUOT		122	; Quota exceeded

%define ENOMEDIUM	123	; No medium found
%define EMEDIUMTYPE	124	; Wrong medium type
%define ECANCELED	125	; Operation Canceled
%define ENOKEY		126	; Required key not available
%define EKEYEXPIRED	127	; Key has expired
%define EKEYREVOKED	128	; Key has been revoked
%define EKEYREJECTED	129	; Key was rejected by service
%define EOWNERDEAD	130	; Owner died
%define ENOTRECOVERABLE	131	; State not recoverable
%define ERFKILL		132	; Operation not possible due to RF-kill
%define EHWPOISON	133	; Memory page has hardware error

;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
