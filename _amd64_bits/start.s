;;
;; start of program execution
;;
;; Copyright 2020 Andrew Apted.
;; Use of this code is governed by an MIT-style license.
;; See the top-level "LICENSE.md" file for the full text.
;;

section .text

global _start
extern main_

_start:
	; TODO grab arguments
	; TODO grab environment variables

	xor	rbp,rbp

	call	main_

	mov	edi,0
	call	exit_

	; not reached
	hlt


exit_:
	; this will not return
	mov	eax,sys_exit
	syscall


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
