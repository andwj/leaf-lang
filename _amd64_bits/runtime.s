;; Copyright 2020 Andrew Apted.
;; Use of this code is governed by an MIT-style license.
;; See the top-level "LICENSE.md" file for the full text.

%include "linux_abi.s"

%include "start.s"
%include "print.s"
