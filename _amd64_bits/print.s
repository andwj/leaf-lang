;;
;; printing to stdout
;;
;; Copyright 2020 Andrew Apted.
;; Use of this code is governed by an MIT-style license.
;; See the top-level "LICENSE.md" file for the full text.
;;

section .text

global  lf__print__char
global  lf__print__int
global  lf__print__hex


lf__print__char:
	push	rbp
	mov	rbp,rsp

	mov	rax,rdi
	call	raw_put_unicode

	leave
	ret


raw_put_unicode:
	; encode unicode char into UTF-8
	cmp	eax,127
	jbe	raw_put_byte

	cmp	eax,0x7FF
	jbe	.pair

	cmp	eax,0xFFFF
	jbe	.triple

	cmp	eax,0x10FFFF
	jbe	.quad

	; just show a question mark
	mov	eax,63
	jmp	raw_put_byte

.pair:
	mov	ecx,eax
	shr	eax,6
	or	eax,0xC0
	call	raw_put_byte

	mov	eax,ecx
	and	eax,63
	or	eax,0x80
	jmp	raw_put_byte

.triple:
	mov	ecx,eax
	shr	eax,12
	or	eax,0xE0
	call	raw_put_byte

	mov	eax,ecx
	shr	eax,6
	and	eax,63
	or	eax,0x80
	call	raw_put_byte

	mov	eax,ecx
	and	eax,63
	or	eax,0x80
	jmp	raw_put_byte

.quad:
	mov	ecx,eax
	shr	eax,18
	or	eax,0xF0
	call	raw_put_byte

	mov	eax,ecx
	shr	eax,12
	and	eax,63
	or	eax,0x80
	call	raw_put_byte

	mov	eax,ecx
	shr	eax,6
	and	eax,63
	or	eax,0x80
	call	raw_put_byte

	mov	eax,ecx
	and	eax,63
	or	eax,0x80

raw_put_byte:
	push	rcx

	; use the stack as our buffer
	push	rax

	mov	edi,STDOUT	; fd
	mov	rsi,rsp		; buf
	mov	edx,1		; count

	mov	eax,sys_write
	syscall

	pop	rax
	pop	rcx

	ret


;-----------------------------------------------------------------------

lf__print__hex:
	push	rbp
	mov	rbp,rsp

	push	rdi
	mov	rax,rdi
	shr	rax,32
	call	raw_put_hex_long

	pop	rax
	call	raw_put_hex_long

	leave
	ret


raw_put_hex_long:
	push	rax
	shr	rax,16
	call	raw_put_hex_word

	pop	rax

raw_put_hex_word:
	push	rax
	shr	rax,8
	call	raw_put_hex_byte

	pop	rax

raw_put_hex_byte:
	push	rax
	shr	rax,4
	call	raw_put_hex_digit

	pop	rax

raw_put_hex_digit:
	and	eax,15
	add	eax,48	; '0'
	cmp	eax,58
	jb	.plain

	add	eax,65-58  ; 'A'
.plain:
	jmp	raw_put_unicode


;-----------------------------------------------------------------------

lf__print__int:
	push	rbp
	mov	rbp,rsp
	push	rbx

	test	rdi,rdi
	jz	.zero
	jns	.positive

	push	rdi
	mov	eax,'-'
	call	raw_put_unicode

	pop	rdi
	neg	rdi

.positive:
	mov	ebx,0  ; becomes set when we get a non-zero digit
	mov	rcx,1000000000000000000

.loop:
	call	raw_put_decimal_digit

	; divide RCX by 10
	xor	rdx,rdx
	mov	rax,rcx
	mov	esi,10

	div	rsi

	mov	rcx,rax
	test	rcx,rcx
	jnz	.loop
	jmp	.done

.zero:
	mov	eax,'0'
	call	raw_put_unicode
.done:
	pop	rbx
	leave
	ret


raw_put_decimal_digit:
	mov	eax,'0'
.loop:
	cmp	rdi,rcx
	jb	.ok

	sub	rdi,rcx
	inc	eax
	or	bl,1
	jmp	.loop
.ok:
	; skip leading zeros
	test	bl,bl
	jz	.done

	push	rcx
	push	rdi

	call	raw_put_unicode

	pop	rdi
	pop	rcx
.done:
	ret


;;--- editor settings -----
;; vi:ts=8:sw=8:noexpandtab
