// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	in_files []string
	out_file string

	win_abi bool

	help bool
}

var out_fp *os.File

// Target is an interface which encapsulates methods for generating
// code for a particular assembler and/or architecture.
type Target interface {
	// Begin is called just after the output file is opened.
	Begin() cmError

	// Finish is called just before the output file is closed.
	Finish() cmError

	// the normal extension for the output file
	DefaultExt() string

	// TODO more stuff....

	ForwardDef(gdef *GlobalDef) cmError
	ProcessDef(gdef *GlobalDef) cmError

	// Identifier encodes the name of an identifier into something
	// which will be valid in the target language.
	Identifier(name string) string

	ConvertType(ty *Type) string
}

var target Target

//----------------------------------------------------------------------

func main() {
	// TODO option for this
	target = NewNasmTarget()

	// this can fatal error
	HandleArgs()

	ClearErrors()

	SetupTypes()
	SetupBuiltins()

	// create output file
	var err error
	out_fp, err = os.Create(Options.out_file)
	if err != nil {
		FatalError("cannot create output: %s", err.Error())
	}

	target.Begin()

	// read all the tokens from every file
	// [ the only errors will be file errors, lexing and bad/dup names ]
	LoadEverything()

	if ! HaveErrors() {
		CompileEverything()
	}

	if ! HaveErrors() {
		SaveNativeCode()
	}

	target.Finish()

	if HaveErrors() {
		ShowAllErrors()
		FatalError("some errors occurred")
	}

	out_fp.Close()

	os.Exit(0)
}

func HandleArgs() {
	Options.in_files = make([]string, 0)

	argv.Generic("o", "output", &Options.out_file, "file", "name of output file")
	argv.Enabler("w", "winabi", &Options.win_abi, "use Windows func-call ABI (not SysV)")
	argv.Gap()
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	names := argv.Unparsed()

	if Options.help || len(names) == 0 {
		ShowUsage()
		os.Exit(0)
	}

	if len(names) == 0 {
		FatalError("missing input filenames")
	}
	Options.in_files = names

	if Options.out_file == "" {
		first := Options.in_files[0]
		bare := FileRemoveExtension(first)
		Options.out_file = bare + "." + target.DefaultExt()
	}
}

func ShowUsage() {
	Print("Usage: yb-leaf FILE.lf [OPTIONS...]\n")

	Print("\n")
	Print("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	if out_fp != nil {
		out_fp.Close()
	}
	format = "yb-leaf: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(1)
}

func Print(format string, a ...interface{}) {
	format = fmt.Sprintf(format, a...)
	fmt.Printf("%s", format)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

//----------------------------------------------------------------------

func LoadEverything() {
	context = NewNamespace()

	// visit all files, even if some produce an error
	for _, in_file := range Options.in_files {
		LoadCodeFile(in_file)
	}
}

func LoadCodeFile(fullname string) cmError {
	visibility_private = true

	file, err := os.Open(fullname)
	if err != nil {
		PostError("no such file: %s", fullname)
		return FAILED
	}

	ErrorSetModule("")
	ErrorSetFile(fullname)

	lex := NewLexer(file)
	lex.sweet = true

	err2 := LoadScanner(lex)

	file.Close()

	return err2
}

func LoadScanner(lex *Lexer) cmError {
	for {
		t := lex.Scan()

		ErrorSetToken(t)

		if t.Kind == TOK_ERROR {
			PostError("%s", t.Str)
			return FAILED
		}

		if t.Kind == TOK_EOF {
			return OKAY
		}

		LoadRawDefinition(t)
	}

	if HaveErrors() {
		return FAILED
	} else {
		return OKAY
	}
}

func LoadRawDefinition(t *Token) cmError {
	ErrorSetToken(t)

	if t.Match("#private") {
		visibility_private = true
		return OKAY
	}
	if t.Match("#public") {
		visibility_private = false
		return OKAY
	}

	if t.Kind == TOK_Expr {
		if len(t.Children) == 0 {
			PostError("empty expr in ()")
			return FAILED
		}

		head := t.Children[0]

		if head.Match("let") {
			return Const_Parse(t)
		}
		if head.Match("type") {
			return Type_CreateRaw(t)
		}
		if head.Match("var") {
			return Glob_CreateRaw(t, 0)
		}
		if head.Match("fun") {
			return Glob_CreateRaw(t, GLOB_FUN)
		}
		if head.Match("zeroed-var") {
			return Glob_CreateRaw(t, GLOB_ZEROED)
		}
		if head.Match("extern-var") {
			return Glob_CreateRaw(t, GLOB_EXTERN)
		}
		if head.Match("extern-fun") {
			return Glob_CreateRaw(t, GLOB_EXTERN|GLOB_FUN)
		}

		if head.Kind == TOK_Name {
			PostError("unknown definition kind '%s'", head.Str)
			return FAILED
		}
	}

	PostError("expected definition, got: %s", t.String())
	return FAILED
}


//----------------------------------------------------------------------

func CompileEverything() {
	// parse all type definitions
	for _, ty := range context.types {
		if !ty.failed {
			Type_ParseDef(ty)
		}
	}

	// check for uninstantiable cyclic types
	for _, ty := range context.types {
		if !ty.failed {
			Type_CheckDef(ty)
		}
	}

	if HaveErrors() {
		return
	}

	// parse all global vars and functions
	for _, gdef := range context.defs {
		Glob_ParseDef(gdef)
	}

	if HaveErrors() {
		return
	}

	// bind identifiers within all global vars and functions
	for _, gdef := range context.defs {
		Glob_BindNames(gdef)
	}

	if HaveErrors() {
		return
	}

	// perform type-checking on all global vars and functions
	for _, gdef := range context.defs {
		Glob_TypeCheck(gdef)
	}

	if HaveErrors() {
		return
	}

	// compile everything to ASM code
	for _, gdef := range context.defs {
		target.ForwardDef(gdef)
	}

	for _, gdef := range context.defs {
		if !gdef.extern {
			target.ProcessDef(gdef)
		}
	}
}

//----------------------------------------------------------------------

func SaveNativeCode() {
	// TODO
}

func OutLine(format string, a ...interface{}) {
	_, err := fmt.Fprintf(out_fp, format, a...)
	if err != nil {
		// TODO abort compilation
	}

	fmt.Fprintf(out_fp, "\n")
}

