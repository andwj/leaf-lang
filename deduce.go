// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

//import "fmt"

type DeduceState struct {
	// for functions, this is the current closure.
	// it is NIL for data expressions.
	cl *Closure
}

func DeduceClosure(cl *Closure) cmError {
	ds := new(DeduceState)
	ds.cl = cl

	if ds.DeduceNode(cl.t_body, cl.ret_type) != OKAY {
		return FAILED
	}

	// check if body is assignable to the return type.
	// [ but if return type is void, we don't care about the body ]
	body_ty := cl.t_body.Info.ty

	if cl.ret_type.base == TYP_Void && body_ty.base != TYP_NoReturn {
		// ok
	} else if body_ty.AssignableTo(cl.ret_type) {
		// ok
	} else if body_ty.base == TYP_Ref && body_ty.sub.AssignableTo(cl.ret_type) {
		// magic deref
		ds.AutoDeref(cl.t_body, cl.ret_type)

	} else {
		PostError("type mismatch on function result: wanted %s, body is %s",
			cl.ret_type.String(), body_ty.String())
		return FAILED
	}

	RejigLocalVars(cl.t_body)

	return OKAY
}

func DeduceGlobalVar(gdef *GlobalDef) cmError {
	t := gdef.tree
	data_type := gdef.ty

	ds := new(DeduceState)

	if ds.DeduceNode(t, data_type) != OKAY {
		return FAILED
	}

	got_type := t.Info.ty

	if !got_type.AssignableTo(data_type) {
		PostError("type mismatch on data expr: wanted %s, got %s",
			data_type.String(), got_type.String())
		return FAILED
	}

	return OKAY
}

//----------------------------------------------------------------------

// DeduceNode visits everything in a code tree or data structure
// tree and gives everything a type.  nodes which already have a
// type (in Info.ty) are assumed to be fully specified AND checked
// already, including all children.  that is the main reason the
// parser does not assign some types to nodes.
//
// for each type of node in a code tree there is usually a
// corresponding DeduceXXX method to handle the type deduction.
// these methods are *guaranteed* to produce a type for a node
// (or produce a compile error).
//
// some DeduceXXX methods take an 'uptype' parameter, which is the
// type which a parent node thinks the child should be.  this is
// particularly important for data structures.  ultimately it is
// the parent's responsibility to check the final type.
//
// there is also a facility to guess the type of a node without
// doing a full deduction on it -- the PartialNode() method.
// it lacks the ability to track types of local variables, so it
// can fail in some common cases, and the programmer may need to
// supply the type information explicitly.
//
func (ds *DeduceState) DeduceNode(t *Token, uptype *Type) cmError {
	if t.Info.ty != nil {
		return OKAY
	}

	ErrorSetToken(t)

	switch t.Kind {
	case TOK_Name:
		if t.IsField() {
			PostError("unexpected field name: %s", t.Str)
		} else {
			PostError("unexpected identifier: %s", t.Str)
		}
		return FAILED

	case ND_Void:
		ds.Deduct(t, void_type)
		return OKAY

	case ND_Pair:
		return ds.DeducePair(t, uptype)

	case ND_Global:
		return ds.DeduceGlobal(t)

	case ND_Local:
		return ds.DeduceLocal(t)

	case ND_Binding:
		return ds.DeduceBinding(t, uptype)

	case ND_Literal:
		return ds.DeduceLiteral(t, uptype)

	case ND_FunCall:
		return ds.DeduceFunCall(t)

	case ND_Builtin:
		return ds.DeduceBuiltin(t)

	case ND_If:
		return ds.DeduceIf(t, uptype)

	case ND_While:
		return ds.DeduceWhile(t)

	case ND_Skip:
		return ds.DeduceSkip(t)

	case ND_Cast:
		return ds.DeduceCast(t)

	case ND_CastPtr:
		return ds.DeduceCastPtr(t)

	case ND_Let:
		return ds.DeduceLet(t, uptype)

	case ND_Var:
		return ds.DeduceVar(t, uptype)

	case ND_ReadRef:
		return ds.DeduceReadRef(t, uptype)

	case ND_WriteRef:
		return ds.DeduceWriteRef(t)

	case ND_SetBinding:
		return ds.DeduceSetBinding(t)

	case ND_AccessPtr:
		return ds.DeduceAccessPtr(t)

	case ND_AccessArray:
		return ds.DeduceAccessArray(t)

	case ND_AccessField:
		return ds.DeduceAccessField(t)

	case ND_DataArray:
		return ds.DeduceDataArray(t, uptype)

	case ND_DataStruct:
		return ds.DeduceDataStruct(t, uptype)

	case ND_DataUnion:
		return ds.DeduceDataUnion(t, uptype)

	default:
		panic("DeduceNode: unexpected node: " + t.String())
	}

	return FAILED
}

//----------------------------------------------------------------------

func (ds *DeduceState) DeducePair(t *Token, uptype *Type) cmError {
	// collect skip nodes as we visit the children
	if t.Info.junction != nil {
		junc := t.Info.junction

		junc.uptype = uptype
		junc.skip_nodes = make([]*Token, 0)
	}

	if ds.DeduceNode(t.Children[0], void_type) != OKAY {
		return FAILED
	}
	if ds.DeduceNode(t.Children[1], uptype) != OKAY {
		return FAILED
	}

	// type checking between skip nodes and the junction.
	// [ this may do a magic deref of the last node ]
	if t.Info.junction != nil {
		if ds.DeduceJunction(t, t.Children[1]) != OKAY {
			return FAILED
		}
	}

	ds.Deduct(t, t.Children[1].Info.ty)
	return OKAY
}

func (ds *DeduceState) DeduceGlobal(t *Token) cmError {
	// node type always mirrors the variable
	ds.Deduct(t, t.Info.gdef.ref_ty)
	return OKAY
}

func (ds *DeduceState) DeduceLocal(t *Token) cmError {
	// node type always mirrors the variable
	ds.Deduct(t, t.Info.lvar.ref_ty)
	return OKAY
}

func (ds *DeduceState) DeduceBinding(t *Token, uptype *Type) cmError {
	// node type always mirrors the binding's value
	ds.Deduct(t, t.Info.lvar.ty)
	return OKAY
}

func (ds *DeduceState) DeduceLiteral(t *Token, uptype *Type) cmError {
	// produce a compile error when a literal is used in a VOID
	// context (such as result of void function), since that is
	// very likely to be a programmer mistake.

	if uptype == nil || uptype.base == TYP_Void || uptype.base == TYP_NoReturn {
		PostError("failed to determine type of literal: %s", t.Info.lit)
		return FAILED
	}

	// literals can never be a reference, so try the target type.
	// the parent node is responsible for final check / magic deref.
	if uptype.base == TYP_Ref {
		uptype = uptype.sub
	}

	// check type of literal is compatible
	if uptype.base == TYP_Int && t.Info.lit_kind == "int" {
		// ok

	} else if uptype.base == TYP_Float &&
		(t.Info.lit_kind == "int" || t.Info.lit_kind == "float") {

		// ok

		// upgrade integer literal to float
		// [ the back-ends may rely on this ]
		ds.UpgradeLiteralToFloat(t)

	} else if uptype.base == TYP_Pointer && t.Info.lit_kind == "ptr" {
		// ok

	} else {
		PostError("type mismatch with literal: wanted %s, got %s",
			uptype.base.String(), t.Info.lit_kind)
		return FAILED
	}

	ds.Deduct(t, uptype)
	return OKAY
}

//----------------------------------------------------------------------

func (ds *DeduceState) DeduceFunCall(t *Token) cmError {
	// no uptype here, there's not much we could do with it

	t_func := t.Children[0]
	params := t.Children[1:]

	if ds.DeduceNode(t_func, nil) != OKAY {
		return FAILED
	}
	fun_type := t_func.Info.ty

	if fun_type.base == TYP_Ref && fun_type.sub.base == TYP_Function {
		// ok
		fun_type = fun_type.sub
	} else {
		PostError("cannot call non-function, got %s", fun_type.String())
		return FAILED
	}

	if len(params) != len(fun_type.param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.param), len(params))
		return FAILED
	}

	for i, t_par := range params {
		par_info := fun_type.param[i]

		if ds.DeduceNode(t_par, par_info.ty) != OKAY {
			return FAILED
		}

		if t_par.Info.ty.AssignableTo(par_info.ty) {
			// ok
		} else if t_par.Info.ty.base == TYP_Ref && t_par.Info.ty.sub.AssignableTo(par_info.ty) {
			// magic deref
			ds.AutoDeref(t_par, par_info.ty)
		} else {
			PostError("type mismatch on parameter '%s': wanted %s, got %s",
				par_info.name, par_info.ty.String(), t_par.Info.ty.String())
			return FAILED
		}
	}

	ds.Deduct(t, fun_type.sub)
	return OKAY
}

func (ds *DeduceState) DeduceBuiltin(t *Token) cmError {
	bu := t.Info.builtin
	params := t.Children

	// some limitations herein:
	//   1. builtins cannot have more than two args
	//   2. if more than one arg, they must be same type
	//   3. result is either same type as args, or a bool (u8)

	if len(params) != bu.args {
		PostError("wrong number of parameters: wanted %d, got %d",
			bu.args, len(params))
		return FAILED
	}

	if bu.args < 1 || bu.args > 2 {
		panic("deduce builtin with strange # of args")
	}

	var L *Token
	var R *Token

	L = params[0]
	if bu.args == 2 {
		R = params[1]
	}

	/* deduce the child nodes */

	var L_down *Type
	var R_down *Type

	if R != nil {
		L_down = ds.PartialNode(R)

		// magic deref
		if L_down != nil && L_down.base == TYP_Ref {
			L_down = L_down.sub
		}
	}

	if ds.DeduceNode(L, L_down) != OKAY {
		return FAILED
	}

	if R != nil {
		R_down = L.Info.ty

		// magic deref
		if R_down != nil && R_down.base == TYP_Ref {
			R_down = R_down.sub
		}

		if ds.DeduceNode(R, R_down) != OKAY {
			return FAILED
		}
	}

	/* check L and R are compatible */

	var L_type *Type
	var R_type *Type

	L_type = L.Info.ty
	if R != nil {
		R_type = R.Info.ty
	}

	// magic deref
	L_deref := false
	R_deref := false

	if L_type != nil && L_type.base == TYP_Ref {
		L_type = L_type.sub
		L_deref = true
	}
	if R_type != nil && R_type.base == TYP_Ref {
		R_type = R_type.sub
		R_deref = true
	}

	if R != nil {
		if L_type.AssignableTo(R_type) {
			// ok
		} else if R_type.AssignableTo(L_type) {
			// ok
		} else {
			PostError("type mismatch between args to '%s': %s and %s",
				bu.name, L_type.String(), R_type.String())
			return FAILED
		}
	}

	/* check intrinsic supports the argument type(s) */

	if (bu.flags & BF_PTR) != 0 && (L_type.base == TYP_Pointer) {
		// ok
	} else if (bu.flags & BF_SIGNED) != 0 && (L_type.base == TYP_Int) && !L_type.unsigned {
		// ok
	} else if (bu.flags & BF_UNSIGNED) != 0 && (L_type.base == TYP_Int) && L_type.unsigned {
		// ok
	} else if (bu.flags & BF_FLOAT) != 0 && (L_type.base == TYP_Float) {
		// ok
	} else {
		PostError("type not supported by '%s' builtin: %s",
			bu.name, L_type.String())
		return FAILED
	}

	// the shift intrinsics require a unsigned count
	if (bu.flags & BF_SHIFT) != 0 && !R_type.unsigned {
		PostError("require unsigned count for shift, got %s",
			L_type.String())
		return FAILED
	}

	// upgrade the magic-deref nodes
	if L_deref {
		ds.AutoDeref(L, L_type)
	}
	if R_deref {
		ds.AutoDeref(R, R_type)
	}

	/* result of intrinsic */

	if (bu.flags & BF_COMPARISON) != 0 {
		ds.Deduct(t, u8_type)

	} else if (bu.flags & BF_SAME_RESULT) != 0 {
		// prefer a custom type
		if R != nil &&
			L_type.custom == "" &&
			R_type.custom != "" {

			ds.Deduct(t, R_type)
		} else {
			ds.Deduct(t, L_type)
		}

	} else {
		panic("intrinsic with weird result")
	}

	return OKAY
}

func (ds *DeduceState) DeduceCast(t *Token) cmError {
	t_val := t.Children[0]

	// destination type is always known
	new_type := t.Info.new_ty

	if ds.DeduceNode(t_val, new_type) != OKAY {
		return FAILED
	}

	// simplify AST when child is a literal number/etc
	if t_val.Kind == ND_Literal {
		t.Replace(t_val)
		return OKAY
	}

	val_type := t_val.Info.ty

	if val_type.CastableTo(new_type) {
		// ok
	} else if val_type.base == TYP_Ref && val_type.sub.CastableTo(new_type) {
		// magic deref
		ds.AutoDeref(t_val, val_type.sub)
	} else {
		PostError("cannot cast from %s to %s",
			val_type.String(), new_type.String())
		return FAILED
	}

	ds.Deduct(t, new_type)
	return OKAY
}

func (ds *DeduceState) DeduceCastPtr(t *Token) cmError {
	t_val := t.Children[0]

	if ds.DeduceNode(t_val, nil) != OKAY {
		return FAILED
	}

	val_type := t_val.Info.ty

	if val_type.base != TYP_Ref {
		PostError("the @ form requires a ref, got %s", val_type.String())
		return FAILED
	}

	new_type := NewType(TYP_Pointer, 0)
	new_type.sub = val_type.sub
	new_type.byte_len = 8

	t.Kind = ND_Cast
	t.Info.new_ty = new_type

	ds.Deduct(t, new_type)
	return OKAY
}

func (ds *DeduceState) DeduceLet(t *Token, uptype *Type) cmError {
	t_exp  := t.Children[0]
	t_body := t.Children[1]

	lvar := t.Info.lvar
	if lvar == nil {
		panic("let form Failed to find variable")
	}

	// we NEED to get a type from the expression
	if ds.DeduceNode(t_exp, nil) != OKAY {
		return FAILED
	}

	exp_type := t_exp.Info.ty

	// determine variable's type
	{
		if exp_type == nil || exp_type.base == TYP_Void || exp_type.base == TYP_NoReturn {
			PostError("could not determine type of let binding '%s'", lvar.name)
			return FAILED
		}

		if t.Info.ref {
			if exp_type.base != TYP_Ref {
				PostError("let-ref must have reference type, got %s",
					exp_type.String())
				return FAILED
			}
		} else {
			// plain `let` will always deref a reference type
			if exp_type.base == TYP_Ref {
				exp_type = exp_type.sub
			}
			if !exp_type.IsBasic() {
				PostError("let binding cannot be complex type: %s",
					exp_type.String())
				return FAILED
			}
		}

		lvar.ty = exp_type
	}

	// magic deref for plain `let` with a reference type.
	// [ DeduceBinding has checked that target type is basic ]
	if !t.Info.ref && exp_type.base == TYP_Ref {
		ds.AutoDeref(t_exp, exp_type.sub)
	}

	// the body is easy
	if ds.DeduceNode(t_body, uptype) != OKAY {
		return FAILED
	}

	ds.Deduct(t, t_body.Info.ty)
	return OKAY
}

func (ds *DeduceState) DeduceVar(t *Token, uptype *Type) cmError {
	t_exp  := t.Children[0]
	t_body := t.Children[1]

	lvar := t.Info.lvar
	if lvar == nil {
		panic("var form faiLed to find variable")
	}

	if lvar.ty.base == TYP_Void || lvar.ty.base == TYP_NoReturn {
		PostError("local variable cannot be void")
		return FAILED
	}

	// now do expression
	if ds.DeduceNode(t_exp, lvar.ty) != OKAY {
		return FAILED
	}

	// FIXME REVIEW THIS FOR DATA-STUFF

	exp_type := t_exp.Info.ty

	if exp_type.AssignableTo(lvar.ty) {
		// ok
	} else if exp_type.base == TYP_Ref && exp_type.sub.AssignableTo(lvar.ty) {
		// magic deref
		ds.AutoDeref(t_exp, exp_type.sub)
	} else {
		PostError("type mismatch in assignment: wanted %s, got %s",
			lvar.ty.String(), exp_type.String())
		return FAILED
	}

	// the body is easy
	if ds.DeduceNode(t_body, uptype) != OKAY {
		return FAILED
	}

	ds.Deduct(t, t_body.Info.ty)
	return OKAY
}

func (ds *DeduceState) DeduceReadRef(t *Token, uptype *Type) cmError {
	t_ref := t.Children[0]

	var downtype *Type

	if uptype != nil {
		downtype = uptype.MakeRefType()
	}

	if ds.DeduceNode(t_ref, downtype) != OKAY {
		return FAILED
	}

	ref_type := t_ref.Info.ty

	if ref_type.base != TYP_Ref {
		PostError("expected ref type, got %s", ref_type.String())
		return FAILED
	}
	if !ref_type.sub.IsBasic() {
		PostError("cannot deref a complex type: %s", ref_type.sub.String())
		return FAILED
	}

	ds.Deduct(t, ref_type.sub)
	return OKAY
}

func (ds *DeduceState) DeduceWriteRef(t *Token) cmError {
	t_ref := t.Children[0]
	t_exp := t.Children[1]

	// I'm fairly sure that producing a downtype here (based on t_exp)
	// could never actually do anything (deduce an unknown node).
	if ds.DeduceNode(t_ref, nil) != OKAY {
		return FAILED
	}

	ref_type := t_ref.Info.ty

	if ref_type.base != TYP_Ref {
		PostError("assignment requires a ref type, got %s", ref_type.String())
		return FAILED
	}

	if ds.DeduceNode(t_exp, ref_type.sub) != OKAY {
		return FAILED
	}

	// check if expression is compatible with destination
	exp_type := t_exp.Info.ty

	if exp_type.AssignableTo(ref_type.sub) {
		// ok
	} else if exp_type.base == TYP_Ref && exp_type.sub.AssignableTo(ref_type.sub) {
		// magic deref
		ds.AutoDeref(t_exp, exp_type.sub)
	} else {
		PostError("type mismatch in assignment: wanted %s, got %s",
			ref_type.sub.String(), exp_type.String())
		return FAILED
	}

	// node itself is always void
	ds.Deduct(t, void_type)
	return OKAY
}

func (ds *DeduceState) DeduceSetBinding(t *Token) cmError {
	lvar := t.Info.lvar

	t_exp := t.Children[0]

	if ds.DeduceNode(t_exp, lvar.ty) != OKAY {
		return FAILED
	}

	// check if expression is compatible with destination
	exp_type := t_exp.Info.ty

	if exp_type.AssignableTo(lvar.ty) {
		// ok
	} else if exp_type.base == TYP_Ref && exp_type.sub.AssignableTo(lvar.ty) {
		// magic deref
		ds.AutoDeref(t_exp, exp_type.sub)
	} else {
		PostError("type mismatch in assignment: wanted %s, got %s",
			lvar.ty.String(), exp_type.String())
		return FAILED
	}

	// node itself is always void
	ds.Deduct(t, void_type)
	return OKAY
}

func (ds *DeduceState) DeduceAccessPtr(t *Token) cmError {
	t_ptr := t.Children[0]

	// I'm quite sure that producing a downtype (based on uptype)
	// could never actually do anything (deduce an unknown node)
	// except maybe give a type to the Null literal.

	if ds.DeduceNode(t_ptr, nil) != OKAY {
		return FAILED
	}

	ptr_type := t_ptr.Info.ty

	if ptr_type.base == TYP_Pointer {
		// ok

	} else if ptr_type.base == TYP_Ref && ptr_type.sub.base == TYP_Pointer {
		// magic deref
		ptr_type = ptr_type.sub
		ds.AutoDeref(t_ptr, ptr_type)

	} else {
		PostError("expected pointer type in access, got %s",
			ptr_type.base.String())
		return FAILED
	}

	ref_type := ptr_type.sub.MakeRefType()

	// convert node to a cast
	t.Kind = ND_Cast
	t.Info.new_ty = ref_type

	ds.Deduct(t, ref_type)
	return OKAY
}

func (ds *DeduceState) DeduceAccessArray(t *Token) cmError {
	t_arr := t.Children[0]
	t_idx := t.Children[1]

	// I'm fairly sure that producing a downtype here (based on uptype)
	// could never actually do anything (deduce an unknown node).

	if ds.DeduceNode(t_arr, nil) != OKAY {
		return FAILED
	}

	arr_type := t_arr.Info.ty

	if !(arr_type.base == TYP_Ref && arr_type.sub.base == TYP_Array) {
		PostError("expected array ref in access, got %s", arr_type.String())
		return FAILED
	}
	arr_type = arr_type.sub

	// handle the index
	if ds.DeduceNode(t_idx, u32_type) != OKAY {
		return FAILED
	}

	idx_type := t_idx.Info.ty

	if idx_type.base == TYP_Int {
		// ok
	} else if idx_type.base == TYP_Ref && idx_type.sub.base == TYP_Int {
		// magic deref
		ds.AutoDeref(t_idx, idx_type.sub)
	} else {
		PostError("expected integer type in array access, got %s",
			idx_type.base.String())
		return FAILED
	}

	elem_type := arr_type.sub
	ref_type  := elem_type.MakeRefType()

	ds.Deduct(t, ref_type)
	return OKAY
}

func (ds *DeduceState) DeduceAccessField(t *Token) cmError {
	t_obj := t.Children[0]

	if ds.DeduceNode(t_obj, nil) != OKAY {
		return FAILED
	}

	// check object is a reference to a struct or union
	obj_type := t_obj.Info.ty

	if obj_type.base != TYP_Ref ||
		!(obj_type.sub.base == TYP_Struct || obj_type.sub.base == TYP_Union) {

		PostError("expected struct/union ref in access, got %s",
			obj_type.String())
		return FAILED
	}
	obj_type = obj_type.sub

	field_idx := obj_type.FindParam(t.Str)
	if field_idx < 0 {
		PostError("no such struct/union field '%s'", t.Str)
		return FAILED
	}

	t.Info.field_idx = field_idx

	field_type := obj_type.param[field_idx].ty
	ref_type   := field_type.MakeRefType()

	ds.Deduct(t, ref_type)
	return OKAY
}

func (ds *DeduceState) DeduceIf(t *Token, uptype *Type) cmError {
	t_cond := t.Children[0]
	t_then := t.Children[1]
	t_else := t.Children[2]

	// handle condition
	if ds.DeduceNode(t_cond, u8_type) != OKAY {
		return FAILED
	}

	cond_ty := t_cond.Info.ty

	if cond_ty.base == TYP_Int {
		// ok
	} else if cond_ty.base == TYP_Ref && cond_ty.sub.base == TYP_Int {
		// magic deref
		ds.AutoDeref(t_cond, cond_ty.sub)
	} else {
		PostError("if condition must be integer, got %s", cond_ty.String())
		return FAILED
	}

	then_ty := t_then.Info.ty
	else_ty := t_else.Info.ty

	// deduce the then body, determine a downtype for it.
	// the uptype is preferred when known, since that matches
	// the pass-through behavior of ND_Pair and ND_Let.
	down_ty := uptype

	if down_ty == nil {
		down_ty = ds.PartialNode(t_else)
	}

	if ds.DeduceNode(t_then, down_ty) != OKAY {
		return FAILED
	}
	then_ty = t_then.Info.ty

	// deduce the else body.
	// again we prefer the uptype if known.
	down_ty = uptype

	if down_ty == nil {
		down_ty = then_ty
	}

	if ds.DeduceNode(t_else, then_ty) != OKAY {
		return FAILED
	}
	else_ty = t_else.Info.ty

	// verify that both clauses have compatible types.
	// if one is no-return, final result is the other clause.
	// if one is a void, final result is always void.
	// if one is a ref and the other isn't, use a magic deref,
	// but when both are references then so is the result.

	if else_ty.base == TYP_NoReturn {
		// ok

	} else if then_ty.base == TYP_NoReturn {
		// ok
		then_ty = else_ty

	} else if then_ty.base == TYP_Void || else_ty.base == TYP_Void {
		// ok
		then_ty = void_type

	} else if else_ty.AssignableTo(then_ty) {
		// ok

	} else if then_ty.AssignableTo(else_ty) {
		// ok
		then_ty = else_ty

	} else if else_ty.base == TYP_Ref && else_ty.sub.AssignableTo(then_ty) {
		// magic deref
		ds.AutoDeref(t_else, else_ty.sub)

	} else if then_ty.base == TYP_Ref && then_ty.sub.AssignableTo(else_ty) {
		// magic deref
		ds.AutoDeref(t_then, then_ty.sub)
		then_ty = else_ty

	} else {
		PostError("type mismatch with 'if' results: %s and %s",
			then_ty.String(), else_ty.String())
		return FAILED
	}

	ds.Deduct(t, then_ty)
	return OKAY
}

func (ds *DeduceState) DeduceWhile(t *Token) cmError {
	t_cond := t.Children[0]
	t_body := t.Children[1]

	if ds.DeduceNode(t_cond, u8_type) != OKAY {
		return FAILED
	}

	if ds.DeduceNode(t_body, void_type) != OKAY {
		return FAILED
	}

	cond_ty := t_cond.Info.ty

	if cond_ty.base == TYP_Int {
		// ok
	} else if cond_ty.base == TYP_Ref && cond_ty.sub.base == TYP_Int {
		// magic deref
		ds.AutoDeref(t_cond, cond_ty.sub)
	} else {
		PostError("while condition must be integer, got %s",
			cond_ty.String())
		return FAILED
	}

	// ignore type of body (it does not matter)

	// node itself is always void
	ds.Deduct(t, void_type)
	return OKAY
}

func (ds *DeduceState) DeduceSkip(t *Token) cmError {
	// this is never NIL here (due to MarkJunctions)
	junc := t.Info.junction

	// add node to the junction's list
	junc.skip_nodes = append(junc.skip_nodes, t)

	t_exp := t.Children[0]

	// if the junction is in tail position, so is t_exp
	if junc.tail {
		MarkTailCalls(t_exp)
	}

	// the expression of the junction's block has not been
	// deduced yet, so cannot use as downtype, but we can pass
	// the block's uptype...

	if ds.DeduceNode(t_exp, junc.uptype) != OKAY {
		return FAILED
	}

	// node itself is no-return
	ds.Deduct(t, no_return_type)
	return OKAY
}

func (ds *DeduceState) DeduceJunction(t_block, last_node *Token) cmError {
	// idea here is that junc.ty becomes the "superset" of all
	// expressions in skip statements and the junction itself.

	junc := t_block.Info.junction

	if len(junc.skip_nodes) == 0 {
		return OKAY
	}

	// step 1: see if we need to deref the junction's value

	junc_type  := last_node.Info.ty
	junc_deref := false

	for _, t_skip := range junc.skip_nodes {
		t_val := t_skip.Children[0]
		val_type := t_val.Info.ty

		if val_type.AssignableTo(junc_type) {
			// ok
		} else if val_type.base == TYP_Ref && val_type.sub.AssignableTo(junc_type) {
			// ok -- will get a deref
		} else if junc_type.base == TYP_Ref && junc_type.sub.AssignableTo(val_type) {
			junc_deref = true
		}
	}

	if junc_deref {
		// magic deref
		junc_type = junc_type.sub
		ds.AutoDeref(last_node, junc_type)
	}

	// step 2: do full check

	for _, t_skip := range junc.skip_nodes {
		t_val := t_skip.Children[0]
		val_type := t_val.Info.ty

		if val_type.AssignableTo(junc_type) {
			// ok
		} else if val_type.base == TYP_Ref && val_type.sub.AssignableTo(junc_type) {
			// magic deref
			ds.AutoDeref(t_val, val_type.sub)
		} else {
			PostError("type mismatch in skip: wanted %s, got %s",
				junc_type.String(), val_type.String())
			return FAILED
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

func (ds *DeduceState) DeduceDataArray(t *Token, uptype *Type) cmError {
	if uptype == nil {
		panic("DeduceDataArray with no type")
	}

	// the parser will create ND_DataArray for structs without fields,
	// so here we need to check if we actuall have a struct type.

	var arr_type *Type

	if uptype.base == TYP_Array {
		// ok
		arr_type = uptype

	} else if uptype.base == TYP_Ref && uptype.sub.base == TYP_Array {
		// ok
		arr_type = uptype.sub

	} else if uptype.base == TYP_Struct ||
		(uptype.base == TYP_Ref && uptype.sub.base == TYP_Struct) {

		// update node kind for the code generator
		t.Kind = ND_DataStruct

		return ds.DeduceDataStruct(t, uptype)

	} else if uptype.base == TYP_Union ||
		(uptype.base == TYP_Ref && uptype.sub.base == TYP_Union) {

		PostError("bad union syntax: missing field name")
		return FAILED

	} else {
		PostError("bad type for {} data: wanted array/struct, got %s",
			uptype.String())
		return FAILED
	}

	/* ok, we have an array */

	// check # of elements
	if arr_type.open {
		// for open arrays, any count is valid

		if t.Info.open {
			PostError("cannot use '...' syntax in open arrays")
			return FAILED
		}

	} else if t.Info.open {
		if len(t.Children) > arr_type.size {
			PostError("too many elements: wanted %d or less, got %d",
				arr_type.size, len(t.Children))
			return FAILED
		}

	} else {
		if len(t.Children) != arr_type.size {
			PostError("wrong number of elements: wanted %d, got %d",
				arr_type.size, len(t.Children))
			return FAILED
		}
	}

	elem_type := arr_type.sub

	// deduce the elements
	for _, t_val := range t.Children {
		if ds.DeduceNode(t_val, elem_type) != OKAY {
			return FAILED
		}

		val_type := t_val.Info.ty

		if val_type.AssignableTo(elem_type) {
			// ok
		} else if val_type.base == TYP_Ref && val_type.sub.AssignableTo(elem_type) {
			// magic deref
			ds.AutoDeref(t_val, val_type.sub)
		} else {
			PostError("type mismatch on array element: wanted %s, got %s",
				elem_type.String(), val_type.String())
			return FAILED
		}
	}

	ds.Deduct(t, arr_type)
	return OKAY
}

func (ds *DeduceState) DeduceDataStruct(t *Token, uptype *Type) cmError {
	if uptype == nil {
		panic("DeduceDataStruct with no type")
	}

	// the parser will create ND_DataStruct for unions too, so
	// we need to check for union types here.

	var obj_type *Type

	if uptype.base == TYP_Struct {
		// ok
		obj_type = uptype

	} else if uptype.base == TYP_Ref && uptype.sub.base == TYP_Struct {
		// ok
		obj_type = uptype.sub

	} else if uptype.base == TYP_Union ||
		(uptype.base == TYP_Ref && uptype.sub.base == TYP_Union) {

		// update node kind for the code generator
		t.Kind = ND_DataUnion

		return ds.DeduceDataUnion(t, uptype)

	} else {
		PostError("bad type for {} data: wanted struct/union, got %s",
			uptype.String())
		return FAILED
	}

	/* ok, we have a struct */

	usable_fields := 0
	for _, par := range obj_type.param {
		// skip padding fields
		if par.name != "" {
			usable_fields += 1
		}
	}

	if t.Info.open {
		if len(t.Children) > usable_fields {
			PostError("too many fields: wanted %d or less, got %d",
				usable_fields, len(t.Children))
			return FAILED
		}
	} else {
		if len(t.Children) != usable_fields {
			PostError("wrong number of fields: wanted %d, got %d",
				usable_fields, len(t.Children))
			return FAILED
		}
	}

	p_idx := 0

	for _, child := range t.Children {
		field_name := ""
		field_idx  := -1

		t_val := child

		// Note: duplicate field names are detected by parsing code

		if child.Kind == ND_DataField {
			t_val = child.Children[0]

			field_name = child.Str
			field_idx  = obj_type.FindParam(field_name)

			if field_idx < 0 {
				PostError("unknown field %s in %s", field_name, obj_type.String())
				return FAILED
			}

		} else {
			// skip padding fields
			for obj_type.param[p_idx].name == "" {
				p_idx++
			}

			field_name = obj_type.param[p_idx].name
			field_idx  = p_idx

			p_idx++
		}

		child.Info.field_idx = field_idx

		field_type := obj_type.param[field_idx].ty

		if ds.DeduceNode(t_val, field_type) != OKAY {
			return FAILED
		}

		val_type := t_val.Info.ty

		if val_type.AssignableTo(field_type) {
			// ok
		} else if val_type.base == TYP_Ref && val_type.sub.AssignableTo(field_type) {
			// magic deref
			ds.AutoDeref(t_val, val_type.sub)
		} else {
			PostError("type mismatch on field %s: wanted %s, got %s",
				field_name, field_type.String(), val_type.String())
			return FAILED
		}
	}

	ds.Deduct(t, obj_type)
	return OKAY
}

func (ds *DeduceState) DeduceDataUnion(t *Token, uptype *Type) cmError {
	if uptype == nil {
		panic("DeduceDataUnion with no type")
	}

	union_type := uptype
	if uptype.base == TYP_Ref {
		union_type = uptype.sub
	}

	// check number of fields...
	if t.Info.open {
		PostError("union data structure cannot use '...'")
		return FAILED
	}
	if len(t.Children) != 1 {
		PostError("union data structure requires a single field")
		return FAILED
	}

	// this is always a ND_DataField
	child := t.Children[0]

	field_name := child.Str
	field_idx  := union_type.FindParam(field_name)

	if field_idx < 0 {
		PostError("unknown field %s in %s", field_name, union_type.String())
		return FAILED
	}

	child.Info.field_idx = field_idx

	t_val := child.Children[0]
	field_type := union_type.param[field_idx].ty

	if ds.DeduceNode(t_val, field_type) != OKAY {
		return FAILED
	}

	// check type of the value
	val_type := t_val.Info.ty

	if val_type.AssignableTo(field_type) {
		// ok
	} else if val_type.base == TYP_Ref && val_type.sub.AssignableTo(field_type) {
		// magic deref
		ds.AutoDeref(t_val, val_type.sub)
	} else {
		PostError("type mismatch on field %s: wanted, %s got %s",
			field_name, field_type.String(), val_type.String())
		return FAILED
	}

	ds.Deduct(t, union_type)
	return OKAY
}

//----------------------------------------------------------------------

func (ds *DeduceState) PartialNode(t *Token) *Type {
	// this tries to determine the most likely type of the node.
	// however it is not equivalent to a full deduction and there
	// are common cases it cannot handle, for example:
	//
	//    (begin (var A s64 123) A)
	//

	// FIXME check if while is a no-return (infinite loop)

	switch t.Kind {
	case ND_Void, ND_While, ND_WriteRef, ND_SetBinding:
		return void_type

	case ND_Skip:
		return no_return_type

	case ND_Cast:
		return t.Info.new_ty

	case ND_Global:
		return t.Info.gdef.ref_ty

	case ND_Local:
		return t.Info.lvar.ref_ty

	case ND_Binding:
		return t.Info.lvar.ty

	case ND_Pair, ND_Var, ND_Let:
		last := t.Children[len(t.Children)-1]
		return ds.PartialNode(last)

	case ND_ReadRef:
		return ds.PartialReadRef(t)

	case ND_If:
		return ds.PartialIf(t)

	case ND_FunCall:
		return ds.PartialFunCall(t)

	case ND_Builtin:
		return ds.PartialBuiltin(t)

	case ND_AccessPtr:
		return ds.PartialAccessPtr(t)

	case ND_AccessArray:
		return ds.PartialAccessArray(t)

	case ND_AccessField:
		return ds.PartialAccessField(t)

	// Not handled:
	//   - ND_Literal: only know rough type (int, float)
	//   - ND_DataXXX: only occurs in initializers, not code

	default:
		return nil
	}
}

func (ds *DeduceState) PartialReadRef(t *Token) *Type {
	child_type := ds.PartialNode(t.Children[0])

	if child_type != nil && child_type.base == TYP_Ref &&
		child_type.sub.IsBasic() {

		return child_type.sub
	}

	return nil
}

func (ds *DeduceState) PartialIf(t *Token) *Type {
	L_type := ds.PartialNode(t.Children[1])
	R_type := ds.PartialNode(t.Children[2])

	if L_type == nil || R_type == nil {
		if R_type != nil {
			return R_type
		} else {
			return L_type
		}
	}

	// if one is no-return, use the other.
	if L_type.base == TYP_NoReturn {
		return R_type
	} else if R_type.base == TYP_NoReturn {
		return L_type
	}

	// one void usually forces other side to be void (dropped value)
	if L_type.base == TYP_Void || R_type.base == TYP_Void {
		return void_type
	}

	// prefer a non-ref type (other side gets a magic deref)
	if L_type.base == TYP_Ref && R_type.base != TYP_Ref {
		return R_type
	}
	if R_type.base == TYP_Ref && L_type.base != TYP_Ref {
		return L_type
	}

	// the final choice is a bit arbitrary
	return L_type
}

func (ds *DeduceState) PartialFunCall(t *Token) *Type {
	t_func := t.Children[0]

	fun_type := ds.PartialNode(t_func)
	if fun_type == nil {
		return nil
	}

	if !(fun_type.base == TYP_Ref && fun_type.sub.base == TYP_Function) {
		return nil
	}
	fun_type = fun_type.sub

	// result type of the function
	return fun_type.sub
}

func (ds *DeduceState) PartialBuiltin(t *Token) *Type {
	bu := t.Info.builtin
	params := t.Children

	if (bu.flags & BF_COMPARISON) != 0 {
		return u8_type
	}

	if (bu.flags & BF_SAME_RESULT) != 0 {
		// cannot deduce on a shift intrinsic's count
		if (bu.flags & BF_SHIFT) != 0 {
			params = params[0:1]
		}

		for _, t_par := range params {
			par_type := ds.PartialNode(t_par)

			if par_type != nil {
				if par_type.base == TYP_Ref {
					par_type = par_type.sub
				}
				return par_type
			}
		}
	}

	return nil
}

func (ds *DeduceState) PartialAccessPtr(t *Token) *Type {
	t_ptr := t.Children[0]

	ptr_type := ds.PartialNode(t_ptr)
	if ptr_type == nil {
		return nil
	}

	if ptr_type.base == TYP_Pointer {
		// ok
	} else if ptr_type.base == TYP_Ref && ptr_type.sub.base == TYP_Pointer {
		// magic deref
		ptr_type = ptr_type.sub
	} else {
		return nil
	}

	return ptr_type.sub.MakeRefType()
}

func (ds *DeduceState) PartialAccessArray(t *Token) *Type {
	t_arr := t.Children[0]

	arr_type := ds.PartialNode(t_arr)
	if arr_type == nil {
		return nil
	}

	if !(arr_type.base == TYP_Ref && arr_type.sub.base == TYP_Array) {
		return nil
	}
	arr_type = arr_type.sub

	elem_type := arr_type.sub

	return elem_type.MakeRefType()
}

func (ds *DeduceState) PartialAccessField(t *Token) *Type {
	t_obj := t.Children[0]

	obj_type := ds.PartialNode(t_obj)
	if obj_type == nil {
		return nil
	}

	if obj_type.base != TYP_Ref ||
		!(obj_type.sub.base == TYP_Struct || obj_type.sub.base == TYP_Union) {
		return nil
	}

	field_idx := obj_type.FindParam(t.Str)
	if field_idx < 0 {
		return nil
	}

	field_type := obj_type.param[field_idx].ty

	return field_type.MakeRefType()
}

//----------------------------------------------------------------------

func (ds *DeduceState) Deduct(t *Token, new_type *Type) {
	if new_type == nil {
		panic("Deduct with nil new_type")
	}
	if t.Info.ty != nil {
		panic("Deduct on already-set node")
	}

	t.Info.ty = new_type
}

func (ds *DeduceState) UpgradeLiteralToFloat(t *Token) {
	// FIXME : handle hexadecimal!! (and other prefixes)

	if t.Info.lit_kind == "int" {
		t.Info.lit += ".0"
		t.Info.lit_kind = "float"
	}
}

func (ds *DeduceState) AutoDeref(t *Token, sub_type *Type) {
	child := NewNode(t.Kind, 0)
	child.Replace(t)
	child.LineNum = t.LineNum

	t.Kind = ND_ReadRef
	t.Info = NewNodeInfo()
	t.Info.ty = sub_type

	t.Children = make([]*Token, 0)
	t.Add(child)
}
