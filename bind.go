// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

//import "fmt"

// NOTE: temporaries are never used more than once

type LocalVar struct {
	name      string
	ty        *Type
	ref_ty    *Type // for `var` form, this is a TYP_Ref for it

	binding   bool  // true for `let` form, parameters, and some local vars
	mutable   bool  // true if can be modified (the `var` form)
	temporary bool  // true for a compiler-created temporary
	addressed bool  // true for `var` with a non-trivial use of its ref
	used      bool  // true if used somewhere

	offset    int   // negative stack offset, 0 if not yet allocated
	reg       Register  // a register value or ON_STACK, can change
}

type LocalGroup struct {
	consts map[string]*Const
	vars   map[string]*LocalVar

	parent *LocalGroup
}

func (group *LocalGroup) Size() int {
	return len(group.vars)
}

type JunctionInfo struct {
	name  string
	used  bool   // true if any `skip` form uses this
	tail  bool   // true if `junction` form is in tail position

	// this only used in MarkJunctions
	parent *JunctionInfo

	// these are only used by the deduce code
	uptype *Type
	skip_nodes []*Token

	// this only used by back-ends
	label string
	c_dest_var string
}

type BindState struct {
	// the local variables which are currently "in scope", including
	// the parameters of the function/method being compiled.  It is a
	// chain via "parent" field (from innermost block out).  Only used
	// during BindClosure(), vars are not looked up after that.
	locals *LocalGroup

	// the current closure, or NIL for data expressions
	cl *Closure
}

//----------------------------------------------------------------------

// BindClosure analyses a code tree and creates LocalVars for each
// local binding which is created.  It also converts bare TOK_Name
// nodes to ND_Local and ND_Global nodes, adorning them with a
// pointer to the corresponding LocalVar or GlobalDef.
func BindClosure(cl *Closure) cmError {
	bs := new(BindState)
	bs.locals = nil
	bs.cl = cl

	bs.PushLocalGroup()
	defer bs.PopLocalGroup()

	for _, t_par := range cl.t_parameters.Children {
		lvar := bs.AddLocal(t_par.Str)
		lvar.ty = t_par.Info.ty
		lvar.binding = true

		cl.parameters = append(cl.parameters, lvar)
	}

	if bs.BindNode(cl.t_body) != OKAY {
		return FAILED
	}

	if MarkJunctions(cl.t_body, nil) != OKAY {
		return FAILED
	}

	MarkTailCalls(cl.t_body)

	return OKAY
}

func BindDataExpr(t *Token) cmError {
	bs := new(BindState)

	return bs.BindNode(t)
}

//----------------------------------------------------------------------

func (bs *BindState) BindNode(t *Token) cmError {
	ErrorSetToken(t)

	switch t.Kind {
	case TOK_Name:
		return bs.BindName(t)

	case ND_Literal, ND_Void:
		return OKAY

	case ND_Let:
		return bs.BindLet(t, true)

	case ND_Var:
		return bs.BindLet(t, false)

	case ND_DefVar:
		PostError("cannot use 'var' in that context")

	case ND_DefFunc:
		PostError("cannot define functions within code")

	default:
		// everything else can use a basic pass
		return bs.BindChildren(t)
	}

	return FAILED
}

func (bs *BindState) BindChildren(t *Token) cmError {
	for _, sub := range t.Children {
		if bs.BindNode(sub) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (bs *BindState) BindName(t *Token) cmError {
	// ignore field/method names, they are resolved in deduce code
	if t.IsField() {
		return OKAY
	}

	if t.Match("_") {
		PostError("cannot use '%s' in that context", t.Str)
		return FAILED
	}

	name := t.Str

	// find name in current scope
	con := bs.LookupConst(name)
	if con != nil {
		t.Kind = ND_Literal
		t.Info.lit = con.value
		t.Info.lit_kind = con.kind
		return OKAY
	}

	lvar := bs.LookupLocal(name)
	if lvar != nil {
		lvar.used = true
		if lvar.binding {
			t.Kind = ND_Binding
		} else {
			t.Kind = ND_Local
		}
		t.Info.lvar = lvar
		return OKAY
	}

	// it must be a global
	gdef := context.GetDef(name)
	if gdef != nil {
		t.Kind = ND_Global
		t.Info.gdef = gdef
		return OKAY
	}

	PostError("unknown identifier: %s", name)
	return FAILED
}

//----------------------------------------------------------------------

func (bs *BindState) BindLet(t *Token, binding bool) cmError {
	bs.PushLocalGroup()
	defer bs.PopLocalGroup()

	t_exp  := t.Children[0]
	t_body := t.Children[1]

	name := t.Str

	if bs.locals.vars[name] != nil || bs.locals.consts[name] != nil {
		PostError("duplicate let/var name '%s'", name)
		return FAILED
	}

	// handle pure constants
	if binding && t_exp.Kind == ND_Literal {
		con := bs.AddConst(name)
		con.value = t_exp.Info.lit
		con.kind = t_exp.Info.lit_kind

		// rejig parse tree
		t.Replace(t_body)

		return bs.BindNode(t_body)
	}

	// create a local variable binding

	// must do expression BEFORE creating the local,
	// so that forms like (let verb verb) work properly.
	if bs.BindNode(t_exp) != OKAY {
		return FAILED
	}

	lvar := bs.AddLocal(name)
	lvar.binding = binding
	lvar.mutable = !binding

	// use any explicitly given type
	lvar.ty = t.Info.ty

	// deduce code needs this cleared
	t.Info.ty = nil

	if !binding {
		lvar.ref_ty = lvar.ty.MakeRefType()
	}

	t.Info.lvar = lvar

	// do body
	return bs.BindNode(t_body)
}

//----------------------------------------------------------------------

func (bs *BindState) PushLocalGroup() *LocalGroup {
	group := new(LocalGroup)
	group.consts = make(map[string]*Const)
	group.vars = make(map[string]*LocalVar)
	group.parent = bs.locals

	bs.locals = group

	return group
}

func (bs *BindState) PopLocalGroup() {
	if bs.locals == nil {
		panic("PopLocalGroup without push")
	}

	bs.locals = bs.locals.parent
}

func (bs *BindState) AddConst(name string) *Const {
	con := new(Const)
	con.name = name

	bs.locals.consts[name] = con
	return con
}

func (bs *BindState) AddLocal(name string) *LocalVar {
	if bs.locals == nil {
		panic("AddLocal when closure has no LocalGroup")
	}

	lvar := new(LocalVar)
	lvar.name = name

	// the special '_' match-all name is never stored in the group,
	// though its LocalVar will get used in various places.
	// TODO review all usage of '_' (add to language spec?)
	if name != "_" {
		bs.locals.vars[name] = lvar
	}

	return lvar
}

func (bs *BindState) LookupConst(name string) *Const {
	for group := bs.locals; group != nil; group = group.parent {
		con := group.consts[name]

		if con != nil {
			return con // found it
		}
	}

	return nil // not found
}

func (bs *BindState) LookupLocal(name string) *LocalVar {
	for group := bs.locals; group != nil; group = group.parent {
		lvar := group.vars[name]

		if lvar != nil {
			return lvar // found it
		}
	}

	return nil // not found
}

//----------------------------------------------------------------------

func MarkJunctions(t *Token, junc *JunctionInfo) cmError {
	if t.Kind == ND_Pair && t.Info.junction != nil {
		t.Info.junction.parent = junc
		junc = t.Info.junction
	}

	if t.Kind == ND_Skip {
		found := false
		for j := junc; j != nil; j = j.parent {
			if t.Str == j.name {
/* DEBUG
	   Print("Marked Junction at: %s\n", t.String())
*/
				t.Info.junction = j
				t.Info.junction.used = true
				found = true
				break
			}
		}
		if !found {
			PostError("junction '%s' not found", t.Str)
			return FAILED
		}
	}

	if t.Children != nil {
		for _, child := range t.Children {
			if MarkJunctions(child, junc) != OKAY {
				return FAILED
			}
		}
	}

	return OKAY
}

func MarkTailCalls(t *Token) {
	switch t.Kind {
	case ND_FunCall:
		t.Info.tail = true

	case ND_Let, ND_Var:
		// body of a let expression
		MarkTailCalls(t.Children[1])

	case ND_Pair:
		// only last element of a pair can be a tail call
		MarkTailCalls(t.Children[1])

		// this is needed in order to support ND_Skip nodes, which
		// are a tail call whenever the corresponding junction is a
		// tail call.  [ ND_Skip nodes get marked elsewhere ]
		if t.Info.junction != nil {
			t.Info.junction.tail = true
		}

	case ND_If:
		// both bodies (t_then, t_else) can be tail calls
		MarkTailCalls(t.Children[1])
		MarkTailCalls(t.Children[2])

/* TODO casts which don't require _any_ work to perform
	case ND_Cast:
		MarkTailCalls(t.Children[0])
*/

	default:
		// for everything else: no tail calls are possible.
	}
}

//----------------------------------------------------------------------

func RejigLocalVars(t *Token) {
	// find local vars which only have trivial use of their ref
	// (via a ND_ReadRef or ND_WriteRef) and convert them to a
	// binding-like variable.
	//
	// NOTE: must be done after deduction, to cope with auto-derefs.
	//

	children := t.Children

	switch t.Kind {
	case ND_Local:
		t.Info.lvar.addressed = true

	case ND_ReadRef, ND_WriteRef:
		if children[0].Kind == ND_Local {
			children = children[1:]
		}
	}

	// recurse down
	for _, child := range children {
		RejigLocalVars(child)
	}

	// variables which have trivial usage of their ref become bindings
	switch t.Kind {
	case ND_Var:
		lvar := t.Info.lvar

		if !lvar.binding && !lvar.addressed {
			// NOTE: we don't change token from ND_Var --> ND_Let
			//       [ since variable is still mutable ]
			lvar.binding = true
			RejigLocals_Upgrade(children[1], lvar)
		}
	}
}

func RejigLocals_Upgrade(t *Token, lvar *LocalVar) {
	for _, child := range t.Children {
		RejigLocals_Upgrade(child, lvar)
	}

	switch t.Kind {
	case ND_ReadRef:
		t_ref := t.Children[0]
		if t_ref.Kind == ND_Local && t_ref.Info.lvar == lvar {
			t.Kind = ND_Binding
			t.Str  = lvar.name
			t.Info.lvar = lvar
			t.Children = t.Children[1:]
		}

	case ND_WriteRef:
		t_ref := t.Children[0]
		if t_ref.Kind == ND_Local && t_ref.Info.lvar == lvar {
			t.Kind = ND_SetBinding
			t.Str  = lvar.name
			t.Info.lvar = lvar
			t.Children = t.Children[1:]
		}
	}
}
