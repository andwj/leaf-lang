// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

//import "os"
//import "fmt"
// import "strings"

type BuiltinFlags int

const (
	BF_PTR BuiltinFlags = (1 << iota)

	BF_SIGNED    // signed integers
	BF_UNSIGNED  // unsigned integers
	BF_FLOAT     // floating point

	BF_SHIFT     // second arg must be unsigned

	BF_COMPARISON  // result type is u8
	BF_SAME_RESULT // result type is same as first arg
)

type Builtin struct {
	name  string
	args  int
	flags BuiltinFlags
}

var builtins map[string]*Builtin

func SetupBuiltins() {
	builtins = make(map[string]*Builtin)

	// Integer arithmetic

	RegisterBuiltin("ineg", 1, BF_SIGNED|BF_SAME_RESULT)
	RegisterBuiltin("iabs", 1, BF_SIGNED|BF_SAME_RESULT)

	RegisterBuiltin("iadd", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("isub", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("imul", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)

	RegisterBuiltin("idivt", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("idivf", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("iremt", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("iremf", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)

	// Floating point arithmetic

	RegisterBuiltin("fneg", 1, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fabs", 1, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("finv", 1, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fsqrt", 1, BF_FLOAT|BF_SAME_RESULT)

	RegisterBuiltin("fadd", 2, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fsub", 2, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fmul", 2, BF_FLOAT|BF_SAME_RESULT)

	RegisterBuiltin("fdiv",  2, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fremt", 2, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fremr", 2, BF_FLOAT|BF_SAME_RESULT)

	RegisterBuiltin("fround", 1, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("ftrunc", 1, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("ffloor", 1, BF_FLOAT|BF_SAME_RESULT)
	RegisterBuiltin("fceil",  1, BF_FLOAT|BF_SAME_RESULT)

	// Bit-wise operations

	RegisterBuiltin("bit-not", 1, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("bit-and", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("bit-or",  2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)
	RegisterBuiltin("bit-xor", 2, BF_SIGNED|BF_UNSIGNED|BF_SAME_RESULT)

	RegisterBuiltin("shift-left",  2, BF_SIGNED|BF_UNSIGNED|BF_SHIFT|BF_SAME_RESULT)
	RegisterBuiltin("shift-right", 2, BF_SIGNED|BF_UNSIGNED|BF_SHIFT|BF_SAME_RESULT)

	// Comparisons

	RegisterBuiltin("eq?", 2, BF_PTR|BF_SIGNED|BF_UNSIGNED|BF_FLOAT|BF_COMPARISON)
	RegisterBuiltin("ne?", 2, BF_PTR|BF_SIGNED|BF_UNSIGNED|BF_FLOAT|BF_COMPARISON)

	RegisterBuiltin("lt?", 2, BF_PTR|BF_SIGNED|BF_UNSIGNED|BF_FLOAT|BF_COMPARISON)
	RegisterBuiltin("le?", 2, BF_PTR|BF_SIGNED|BF_UNSIGNED|BF_FLOAT|BF_COMPARISON)
	RegisterBuiltin("gt?", 2, BF_PTR|BF_SIGNED|BF_UNSIGNED|BF_FLOAT|BF_COMPARISON)
	RegisterBuiltin("ge?", 2, BF_PTR|BF_SIGNED|BF_UNSIGNED|BF_FLOAT|BF_COMPARISON)

	RegisterBuiltin("null?", 1, BF_PTR|BF_COMPARISON)
	RegisterBuiltin("ref?",  1, BF_PTR|BF_COMPARISON)

	RegisterBuiltin("inf?", 1, BF_FLOAT|BF_COMPARISON)
	RegisterBuiltin("nan?", 1, BF_FLOAT|BF_COMPARISON)
}

func RegisterBuiltin(name string, args int, flags BuiltinFlags) {
	bu := new(Builtin)
	bu.name = name
	bu.args = args
	bu.flags = flags

	builtins[name] = bu
}
