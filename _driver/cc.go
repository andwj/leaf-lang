// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"

func RunLeafCompiler() error {
	var err error

	tool := "./leaf"
	cmd  := exec.Command(tool)

	for _, srcfile := range Options.sources {
		cmd.Args = append(cmd.Args, srcfile)
	}

	cmd.Args = append(cmd.Args, "-o")
	cmd.Args = append(cmd.Args, Work.s_file)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the leaf compiler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box
	cmd.Stdout = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("the leaf code contained errors")
	}
fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

	return Ok
}

//----------------------------------------------------------------------

func RunAssembler() error {
	var err error

	tool := "nasm"
	cmd  := exec.Command(tool, "-f", "elf64", Work.s_file, "-o", Work.o_file)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the assembler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("the generated ASM code failed to assemble")
	}

fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

	return Ok
}

//----------------------------------------------------------------------

func RunLinker() error {
	var err error

	runtime := "_amd64_bits/runtime.o"

	tool := "ld"
	cmd  := exec.Command(tool, runtime, Work.o_file, "-o", Options.exe_name)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the linker
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("the compiled .o files failed to link")
	}

fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

	return Ok
}

//----------------------------------------------------------------------

func RunCCompiler() error {
	var err error

	runtime := "_c_bits/runtime.o"

	tool := "cc"
	cmd  := exec.Command(tool, "-I.", "-Wall", Work.c_file, runtime, "-o", Options.exe_name)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the C compiler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	if err != nil {
		// TODO do something better with errors
		fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

		return fmt.Errorf("the generated C code failed to compile")
	}

fmt.Fprintf(os.Stderr, "%s\n", err_box.String())

	return Ok
}
