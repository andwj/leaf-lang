// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "strings"
import "path/filepath"
// import "fmt"

var Ok error

func FileExists(fn string) bool {
	f, err := os.Open(fn)
	if err == nil {
		f.Close()
	}
	return (err == nil)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

func DirCreate(path string) error {
	if path == "." {
		return Ok
	}
	// don't produce an error if dir already exists
	_ = os.Mkdir(path, 0777)
	return Ok
}
