// Copyright 2020 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "runtime"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	sources   []string
	build_dir string
	exe_name  string
	help      bool
}

var Work struct {
	c_file string
	s_file string
	o_file string
}

func main() {
	var err error

	Options.sources = make([]string, 0)
	Options.build_dir = "_temp"

	argv.Generic("o", "output", &Options.exe_name, "file", "name of output executable")
	argv.Generic("d", "dir", &Options.build_dir, "dir", "name of build dir (_temp)")
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err = argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	names := argv.Unparsed()

	if Options.help || len(names) == 0 {
		ShowUsage()
		os.Exit(0)
	}

	Options.sources = names

	// default executable name if unset
	if Options.exe_name == "" {
		base := FileRemoveExtension(Options.sources[0])
		base = filepath.Base(base)
		if runtime.GOOS == "windows" {
			base += ".exe"
		}
		Options.exe_name = filepath.Join(Options.build_dir, base)
	}

	err = BuildStuff()
	if err != nil {
		FatalError("%s", err.Error())
	}

	os.Exit(0)
}

func BuildStuff() error {
	var err error

	bare_exe := FileRemoveExtension(Options.exe_name)
	bare_exe = filepath.Base(bare_exe)

	Work.c_file = filepath.Join(Options.build_dir, bare_exe + ".c")
	Work.s_file = filepath.Join(Options.build_dir, bare_exe + ".s")
	Work.o_file = filepath.Join(Options.build_dir, bare_exe + ".o")

	err = DirCreate(Options.build_dir)
	if err != nil {
		return err
	}

	// step 1: run the leaf compiler
	err = RunLeafCompiler()
	if err != nil {
		return err
	}

	// step 2: run the assembler (NASM)
	err = RunAssembler()
	if err != nil {
		return err
	}

	// step 3: link the final program
	err = RunLinker()
	if err != nil {
		return err
	}

	Message("successfully compiled %s\n", Options.exe_name)
	return Ok
}

func ShowUsage() {
	Message("Usage: lfcc FILE... [OPTIONS...]")
	Message("")
	Message("Available options:")
	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	format = "lfcc: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(1)
}

func Message(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Printf("\n")
}
