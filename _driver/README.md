
Driver
======

This directory contains a small command-line program to
simplify compiling a leaf program to an executable.  It
takes care of running the leaf compiler, then running the
assembler (NASM) and finally linking to an executable.

Usage is like this:

```
lfcc FILE.lf ... -o EXENAME
```

Multiple leaf input files can be given.
If the output executable filename is not given, the name
is derived from the first input file.

